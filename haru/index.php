<?php require('../cms/wp-load.php'); ?>
<?php
$root_path = "./";
$title = "【公式】石窯パンマルシェ HARU";
$description = "地元ニセコの食材や人が自然と集まる場所＝マルシェ（市場）を目指し、毎日焼き立てのパンを通じてニセコを発信！羊蹄山を一望するテラス＆カフェも併設し、観光やドライブの休憩スポットとしてもぜひお立ち寄りください。";
$keyword = "ハル,HARU,北海道,ニセコ,虻田郡,羊蹄山,駐車場,焼きたてパン,カフェ,スペイン石釜,豆乳,ソフトクリーム,湧水コーヒー,スイーツ,イートインスペース,テラス,絶景";
$ranking_postid = 17;
$osusume_postid = 45;

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_index" class="drawer drawer--right drawer-close">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
  <?php include_once "header.php"; ?>
  <div id="contents">

    <section id="mainvisual" class="mainimg">
        <div class="vegas-pager">
        </div>
    </section>
    <!-- mainvisual -->

    <section class="about_top pt pb">
      <div class="wrapper">
        <h2 class="img_center"><img src="./images/index_about_title.svg?v=20181212" alt="北海道ニセコ町 | 石窯パンマルシェ | HARU"></h2>
          <div class="flexcol photo_right pt cf">
            <div class="photo" style="background:url(./images/index_top_photo1.jpg) no-repeat;">
            </div>
            <!-- photo -->
            <div class="text bg_white googtrans-box">
                アイヌ語で「豊かな食の恵み」。<br class="pc">
                それが石窯パンマルシェ HARU（ハル）の由来です。<br class="pc">
                ここニセコエリアは、野菜やお肉など食材の宝庫。<br class="pc">
                四季折々の豊かな食材を使ったパンを作り、<br class="pc">
                たくさんのお客さまに笑顔になってもらいたい。<br class="pc">
                ぜひ、ニセコにお越しの際はお立ち寄りください。
            </div>
            <!-- text -->
          </div>
          <!-- grid-col2 -->

        <div class="flexcol photo_left cf">
          <div class="photo" style="background:url(./images/index_top_photo2.jpg) no-repeat;">
          </div>
          <!-- photo -->
          <div class="text bg_white googtrans-box">
            「羊蹄山」が一望できるロケーション。<br class="pc">
            天気の良い日はテラス席がおすすめです。<br class="pc">
            イートインコーナーもご用意しています。<br class="pc">
            ドライブの休憩に焼きたてパンはいかがですか？<br class="pc">
            道の駅ニセコビュープラザのすぐ隣です&#9834;
            </div>
          <!-- text -->
        </div>
        <!-- grid-col2 -->
        <p class="linkbtn1 pt pb_l"><a href="./kodawari.php">こだわりについて</a></p>
      </div>
      <!-- wrapper -->
    </section>
    <!-- about-top -->

    <section class="about_bottom pb_l">
      <div class="wrapper">
        <img src="./images/index_mitsuko_photo.jpg?v=20190129" alt="Mitsuko">
        <ul class="pt_l grid_col3 cf">
          <li class="col">
            <div class="matchheight box bg_white">
              <h3 class="title pb_s">手作りの石窯！</h3>
              <img class="img_center" src="./images/index_bottom_photo1.jpg">
              <p class="text pt_s fsize_s sub-matchheight">スペインの石窯職人が来て積み上げた石窯。遠赤外線効果で、ふっくらと焼きあがります。</p>
            </div>
            <!-- box -->
          </li>
          <li class="col">
            <div class="matchheight box bg_white">
              <h3 class="title pb_s">種類が豊富！</h3>
              <img class="img_center" src="./images/index_bottom_photo2.jpg">
              <p class="text pt_s fsize_s sub-matchheight">パンだけで100種類のラインナップです。時間や売れいきにより商品が少ない時間帯があります。</p>
            </div>
            <!-- box -->
            </li>
          <li class="col">
            <div class="matchheight box bg_white">
              <h3 class="title pb_s">絶景テラス席！</h3>
              <img class="img_center" src="./images/index_bottom_photo3.jpg">
              <p class="text pt_s fsize_s sub-matchheight">羊蹄山を望めるテラス席（店内イートインも可）。湧水を使用したコーヒーもご用意しています。</p>
            </div>
            <!-- box -->
          </li>
        </ul>
        <p class="linkbtn1 pt"><a href="./shop.php">店舗案内はこちら</a></p>
      </div>
      <!-- wrapper -->
    </section>
    <!-- about-bottom -->

    <?php if( have_rows('人気ランキング',$ranking_postid)): ?>
    <section class="ranking pt">
      <h2 class="img_center"><img class="pb_s" src="./images/index_ranking_title.svg" alt="Ranking | 人気ランキング"></h2>       
   
      <div class="outer bg_orange pt_l pb">
        <div class="wrapper">
          <ul class="grid_col3 cf">
            <?php $rank_num = 1; ?>
            <?php while( have_rows('人気ランキング',$ranking_postid) ): the_row(); ?>
            <li class="col rank<?php echo $rank_num; ?>">
              <?php $ranking_img = get_sub_field('画像'); ?>
              <img class="img_center" src="<?php echo $ranking_img['sizes']['ranking_img'];?>" alt="<?php echo the_title(); ?>">
              <p class="text pt_s"><?php echo get_sub_field('タイトル'); ?></p>
            </li>
            <?php $rank_num++; ?>
            <?php
              if($rank_num==4) {
                break;
              }
            ?>
            <?php endwhile;
             ?>
          </ul>
        </div>
        <!-- wrapper -->
      </div>
      <!-- outer -->     
    </section>
    <!-- ranking -->
    <?php endif; ?>

      
    <?php if( have_rows('ハールくんおすすめランキング',$osusume_postid)): ?>
    <section class="ranking osusume pt">
      <h2 class="img_center"><img class="pb_s" src="./images/index_osusume_title<?php mobile_img(); ?>.svg?v=20181212" alt="Reccomend | ハールくんおすすめランキング"></h2>       
   
      <div class="outer bg_img1 pt_l pb">
        <div class="wrapper">
          <ul class="grid_col3 cf">
            <?php $rank_num = 1; ?>
            <?php while( have_rows('ハールくんおすすめランキング',$osusume_postid) ): the_row(); ?>
            <li class="col rank<?php echo $rank_num; ?>">
              <?php $ranking_img = get_sub_field('画像'); ?>
              <img class="img_center" src="<?php echo $ranking_img['sizes']['ranking_img'];?>" alt="<?php echo the_title(); ?>">
              <p class="text pt_s"><?php echo get_sub_field('タイトル'); ?></p>
            </li>
            <?php $rank_num++; ?>
            <?php
              if($rank_num==4) {
                break;
              }
            ?>
            <?php endwhile;
             ?>
          </ul>
        </div>
        <!-- wrapper -->
      </div>
      <!-- outer -->     
    </section>
    <!-- ranking -->
    <?php endif; ?>
      

    <section class="voice">
      <div class="outer wrapper">
        <p class="naname pb_l pt_s">姉妹店の豆腐屋さん「湧水の里豆腐工房」の<br>オカラや豆乳を使用したパンもおすすめです&#9834;</p>
        <p class="linkbtn1 mt_l"><a href="./menu.php">焼きたてパンメニューはこちら</a></p>
      </div>
      <!-- wrapper -->
    </section>
    <!-- voiece -->


    <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '4', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC',
      'category_name' => 'haru' // 表示したいカテゴリーのスラッグを指定
    );
    $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>

    <section class="topics bg_gray2 pt_l pb_l">
      <div class="wrapper">
        <h2 class="img_center"><img class="pb_l" src="./images/index_topics_title.svg" alt="Topics | 最新情報"></h2>       

        <?php while($wp_query->have_posts()) :?>
        <?php $wp_query->the_post(); ?>

        <dl class="cf">
          <dt>
            <?php if(has_post_thumbnail()): ?>
              <?php the_post_thumbnail('thumbnail'); ?>
            <?php else: ?>
                <img src="./images/noimg.jpg" alt="no image">
            <?php endif; ?>
          </dt>
          <a href="<?php the_permalink() ?>">
          <dd>
            <p class="date fsize_s"><?php the_time('Y.m.d'); ?></p>
            <h3 class="fsize_l"><?php echo $post->post_title; ?></h3>
            <div class="text tuduki">
              <?php the_excerpt(); ?>
            </div>
          </dd>
          </a>
        </dl>
        <?php endwhile; ?>

        <?php endif; ?>
        <?php wp_reset_query(); ?>

        <p class="linkbtn2 mt_s mb_l"><a href="<?php bloginfo('url'); ?>/category/haru/">もっと見る</a></p>

      </div>
      <!-- wrapper -->
    </section>
    <!-- topics -->

    <section class="access pt_l">
        <h2 class="img_center pb"><img src="./images/index_access_title.svg" alt="Access | 店舗情報"></h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2927.489273548453!2d140.70026936584375!3d42.79914572915986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0aaf691486d455%3A0xc30fcd623cf49703!2z55-z56qv44OR44Oz44Oe44Or44K344KnIEhBUlU!5e0!3m2!1sja!2sjp!4v1539770549656" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div class="store_photo"></div>
    </section>
    <!-- access -->

  </div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>