<footer>

    <div class="fotter_top bg_gray1 pt pb">
        <div class="wrapper">
            <ul class="footer_outer grid_col2 ptn2 cf">
                <li class="col comp_info">
                    <div class="outer cf">
                        <div class="left">
                            <a href="#"><img src="<?php echo $root_path; ?>images/footer_logo.svg"></a>
                        </div>
                        <div class="right pt_s">
                            <h3>石窯パンマルシェ<br>HARU</h3>
                        </div>
                    </div>
                    <!-- outer -->
                    <address class="pt_s">
                        <!--
                <p class="ad"><span class="title">住　　所：</span><a href="https://www.google.com/maps?ll=42.798752,140.702465&z=16&t=m&hl=ja&gl=JP&mapclient=embed&cid=14055678783859758851" target="_blank">北海道虻田郡ニセコ町元町55-1道の駅ニセコビュープラザ横</a></p>
                  <p x-ms-format-detection="none"><span class="title">電話番号：</span>0136-55-6301</p>
                  <p><span class="title">営業時間：</span>夏期（4月～10月）8:00～18:00<br>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;&nbsp;&nbsp;冬期（11月～3月）9:00～17:00</p>
                  <p><span class="title">定 休 日&nbsp;&nbsp;：</span>火曜日（祝日の場合は翌水曜日）、年末年始</p>
                  <p><span class="title">駐 車 場&nbsp;&nbsp;：</span>40台</p>
                  <p><span class="title">座　席&nbsp;&nbsp;&nbsp;&nbsp;：</span>40席</p>
-->
                        <dl class="cf">
                            <dt>住　　所：</dt>
                            <dd><a href="https://www.google.com/maps?ll=42.798752,140.702465&z=16&t=m&hl=ja&gl=JP&mapclient=embed&cid=14055678783859758851" target="_blank">北海道虻田郡ニセコ町元町55-1道の駅ニセコビュープラザ横</a></dd>
                        </dl>
                        <dl class="cf">
                            <dt>電話番号：</dt>
                            <dd x-ms-format-detection="none"><a href="tel:0136-55-6301">0136-55-6301</a></dd>
                        </dl>
                        <dl class="cf">
                            <dt>営業時間：</dt>
                            <dd>夏期（4月～10月）平日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10:00～18:00
                                <span class="right">土日祝&nbsp;&nbsp;&nbsp;&nbsp;9:00～18:00</span><span class="season">冬期（11月～3月）全日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10:00～17:00</span></dd>
                        </dl>
                        <dl class="cf">
                            <dt>定&nbsp;&nbsp;休&nbsp;&nbsp;日：</dt>
                            <dd>年末年始</dd>
                        </dl>

                        <dl class="cf">
                            <dt>駐&nbsp;&nbsp;車&nbsp;&nbsp;場：</dt>
                            <dd>40台</dd>
                        </dl>
                        <dl class="cf">
                            <dt>座　　席：</dt>
                            <dd>イートイン 24席　テラス 24席</dd>
                        </dl>
                    </address>
                </li>
                <!-- col -->
                <li class="col other_store">
                    <h3 class="headline3 pb_s">お近くにお越しの際はぜひお立ち寄りください</h3>
                    <ol class="grid_col2 ptn2 cf pt_s">
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../wakimizunosato/" target="_blank">
                                <img class="sp" src="<?php echo $root_path; ?>images/footer_str_wakimizu_sp.jpg">
                                <img class="pc" src="<?php echo $root_path; ?>images/footer_str_wakimizu.jpg">
                                <p>おいしい豆腐づくりを追求しています。お土産にも最適です。</p>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../yoteizan/" target="_blank">
                                <img class="sp" src="<?php echo $root_path; ?>images/footer_str_youteizan_sp.jpg">
                                <img class="pc" src="<?php echo $root_path; ?>images/footer_str_youteizan.jpg">
                                <p>自然の中で味わう手打ちそば屋です。</p>
                            </a>
                        </li>
                    </ol>

                </li>
                <!-- col -->
            </ul>
        </div>
        <!-- wrapper -->
    </div>
    <!-- footer-top -->

    <div class="fotter-bottom">

        <div class="wrapper">
            <nav>
                <ul class="sp cf">
                    <li class="sp"><a href="<?php echo $root_path; ?>index.php">トップページ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>menu.php#top">焼きたてパンメニュー</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>menu.php#cafemenu">カフェメニュー</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>shop.php">店舗情報</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
                <ul class="comp cf">
<!--
                    <li class="text"><a href="#">採用情報</a></li>
                    <li class="text"><a href="#">会社情報</a></li>-->
                    <li class="text"><a href="#">プライバシーポリシー</a></li>
                </ul>
            </nav>
            <p class="copy">Copyright &copy; WAKIMIZUNOSATO Co., Ltd. All Right Reserved.</p>
        </div>
    </div>
    <!-- footer-bottom -->

</footer>

<div id="page_top">

    <a href="#"><img src="<?php echo $root_path; ?>images/top.svg" alt="pagetop"></a>
</div>

<script type="text/javascript" src="<?php echo $root_path; ?>js/top.js"></script>

</main>
