<section id="bottom_link">
<div class="wrapper">
  <ul class="grid_col3 cf pb">

    <li class="col">
        <a href="<?php echo $root_path; ?>kodawari.php">
            <div class="photo photo1">
                <img src="./images/bottom_link_photo1.jpg">
                <div class="text">HARUのこだわり</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>ニセコ産の豊かな食材を、職人こだわりのレシピでお届け。</p>
    </li>
    <li class="col">
        <a href="<?php echo $root_path; ?>shop.php">
            <div class="photo photo2">
                <img src="./images/bottom_link_photo2.jpg">
                <div class="text">店舗情報</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>焼き立てパンをその場で味わえるカフェ＆テラス</p>
    </li>
    <li class="col">
        <a href="<?php echo $root_path; ?>shop.php#access">
            <div class="photo photo3">
                <img src="./images/bottom_link_photo3.jpg">
                <div class="text">アクセス</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>ニセコビュープラザ前。お店へのアクセスはこちら</p>
    </li>
  </ul>
</div>
</section>