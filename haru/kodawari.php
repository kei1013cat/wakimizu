<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "HARUのこだわり｜【公式】石窯パンマルシェ HARU";
$description = "地元の食材を使ったパンで、新しい発見や驚きを。地元の方も、観光の方も「HARUに行ったらワクワクする♪笑顔になる♪あったかい♪」。そんなお店を私たちは目指しています。";
$keyword = "ハル,HARU,北海道,ニセコ,虻田郡,羊蹄山,駐車場,焼きたてパン,カフェ,スペイン石釜,豆乳,ソフトクリーム,湧水コーヒー,スイーツ,イートインスペース,テラス,絶景";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_kodawari" class="subpage drawer drawer--right drawer-close">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
  <?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">
  <?php include_once "pan.php"; ?>

<section class="omoi pt_l pb_l">
<div class="wrapper">
  <h2 class="headline1 mb">HARUの想い</h2>
  <ul class="grid_col2 ptn2 cf">
    <li class="col"><img src="./images/kodawari_omoi_photo1.jpg"></li>
    <li class="col"><img src="./images/kodawari_omoi_photo2.png"></li>
  </ul>
  <!-- col2 -->
  <div class="text">
    <h3 class="fsize_l mb_s">地元の食材や人が自然と集まる場所＝マルシェ（市場）<br class="pc">パンを通じてニセコを発信！お客様を笑顔にしたい。</h3>
    <p>ここニセコエリアは北海道を代表する観光地、そして食材の宝庫です。地元の食材や、人が賑やかに集まるマルシェ（市場）、そんな場所にしたいと考えています。地元の食材を使ったパンで、新しい発見や驚きを。地元の方も、観光の方も「HARUに行ったらワクワクする♪笑顔になる♪あったかい♪」。そんなお店を私たちは目指しています。</p>
  </div>
  <!-- text -->
</div>
<!-- wrapper -->
</section>
<!-- omoi -->

<section class="kodawari5 pt">
<div class="wrapper"><h2 class="headline1 mb">HARUの<span class="green large">5</span>つのこだわり</h2></div>
<div class="bg_gray2 cf pt pb">
  <div class="wrapper">
    <div class="flexcol photo_left cf">
      <div class="photo">
        <img src="./images/kodawari_sousaku_photo.jpg">
      </div>
      <!-- photo -->
      <div class="text cf">
        <h3 class="mb">地元食材を使用した創作パン</h3>
        <p>HARUの自慢は地元の食材を使用した創作パン。かぼちゃや、じゃがいも、トマト、ゴボウ、アスパラ、スイートコーン、ブロッコリーなど、できる限り地元の食材を使用した創作パンを日々開発しています。※季節により使用する食材は変わります。</p>
      </div>
      <!-- text -->
    </div>
    <!-- flexcol -->
  </div>
  <!-- wrapper -->
</div>
<!-- col2 -->

<div class="col2 ptn2 cf pt pb">
  <div class="wrapper">
    <div class="flexcol photo_right cf">
      <div class="photo">
        <img src="./images/kodawari_space_photo.jpg">
      </div>
      <!-- photo -->
      <div class="text">
        <h3 class="mb_s">「焼きたて」を味わう<br class="sp">イートインコーナー</h3>
        <p>HARUにはテラス席に加えて、焼きたてパンをその場で食べることができるイートインコーナーをご用意しております。湧水を使用したコーヒーや、湧水の里豆腐工房さんの豆乳を使用したソフトクリームなどのカフェメニューも。</p>
      </div>
      <!-- text -->
    </div>
    <!-- flexcol -->
  </div>
  <!-- wrapper -->
</div>
<!-- col2 -->

<div class="col2 ptn2 bg_gray2 cf pt pb">
  <div class="wrapper">
    <div class="flexcol photo_left cf">
      <div class="photo">
        <img src="./images/kodawari_ishigama_photo.jpg">
      </div>
      <!-- photo -->
      <div class="text">
        <h3 class="mb_s">本場スペインの職人作った石窯</h3>
        <p>スペインの職人が来日し、設計、手積みした手作り石窯をHARUでは使用しています。遠赤外線効果で、短時間で焼き上げられるので、うすい皮なのに、中がふっくらと焼きあがるのが特徴なんです。</p>
      </div>
      <!-- text -->
    </div>
    <!-- flexcol -->
  </div>
  <!-- wrapper -->
</div>
<!-- col2 -->

<div class="col2 ptn1 cf pt pb">
  <div class="wrapper">
    <div class="flexcol photo_right cf">
      <div class="photo">
        <img src="./images/kodawari_menu_photo.jpg">
      </div>
      <!-- photo -->
      <div class="text">
        <h3 class="mb_s">豊富なメニュー数は100種類！</h3>
        <p>パンだけで100種類ほどのメニューラインナップをご用意しています。その季節ならではの限定メニューも日々研究中。仕込める量や、石窯の状況によりますが、60種類ほどのパンを店内で販売しています。※時間帯や、売れいきによりメニュー数が少ないこともございます。</p>
      </div>
      <!-- text -->
    </div>
    <!-- flexcol -->
  </div>
  <!-- wrapper -->
</div>
<!-- col2 -->

<div class="col2 ptn2 bg_gray2 cf pt pb">
  <div class="wrapper">
    <div class="flexcol photo_left cf">
      <div class="photo">
        <img src="./images/kodawari_scratch_photo.jpg">
      </div>
      <!-- photo -->
      <div class="text">
        <h3 class="mb_s">こだわりの店内仕込み</h3>
        <p>複数の小麦粉をブレンドし、生地の仕込みから発酵、焼き上げまでを店内で行うことはもちろん、創作パンに使用する具材や、カレー、クリームなどもできる限り店内で仕込み手作りにこだわっています。</p>
      </div>
      <!-- text -->
    </div>
    <!-- flexcol -->
  </div>
  <!-- wrapper -->
</div>
<!-- col2 -->

</section>
<!-- kodawari5 -->

<section id="bottom_link">
<div class="wrapper">
  <ul class="grid_col3 cf pb">

    <li class="col">
        <a href="<?php echo $root_path; ?>menu.php">
            <div class="photo photo1">
                <img src="./images/bottom_link_photo4.jpg">
                <div class="text lg">焼き立てパンメニュー</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>地元食材を使用した種類豊富なメニューを紹介</p>
    </li>
    <li class="col">
        <a href="<?php echo $root_path; ?>shop.php">
            <div class="photo photo2">
                <img src="./images/bottom_link_photo2.jpg">
                <div class="text">店舗情報</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>焼き立てパンをその場で味わえるカフェ＆テラス</p>
    </li>
    <li class="col">
        <a href="<?php echo $root_path; ?>shop.php#access">
            <div class="photo photo3">
                <img src="./images/bottom_link_photo3.jpg">
                <div class="text">アクセス</div>
                <div class="bg"></div>
            </div>
        </a>
        <p>ニセコビュープラザ前。お店へのアクセスはこちら</p>
    </li>
  </ul>
</div>
</section>

  </div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>