// JavaScript Document
jQuery(function($){
 
    // 「ページトップへ」の要素を隠す
    //$('#page_top1').css("display" , "none");
 
    // スクロールした場合
    $(window).scroll(function(){
        // スクロール位置が100を超えた場合
        if ($(this).scrollTop() > 100) {
            // 「ページトップへ」をフェードイン
            $('#page_top').fadeIn();
        }
        // スクロール位置が100以下の場合
        else {
            // 「ページトップへ」をフェードアウト
            $('#page_top').fadeOut();
        }
    });
 
    // 「ページトップへ」をクリックした場合
    $('#page_top').click(function(){
        // ページトップにスクロール
        $('html,body').animate({
            scrollTop: 0
        }, 350);
        return false;
    });
 
});