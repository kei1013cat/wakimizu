<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "HARUのメニュー｜【公式】石窯パンマルシェ HARU";
$description = "HARUの自慢は、毎日焼き立てをお届けする、地元の食材を使用した創作パン。かぼちゃや、じゃがいも、トマト、ゴボウ、アスパラ、スイートコーン、ブロッコリーなど、できる限り地元の食材を使用した創作パンを日々開発しています。";
$keyword = "ハル,HARU,北海道,ニセコ,虻田郡,羊蹄山,駐車場,焼きたてパン,カフェ,スペイン石釜,豆乳,ソフトクリーム,湧水コーヒー,スイーツ,イートインスペース,テラス,絶景";
$ranking_postid = 17;
$osusume_postid = 45;

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_menu" class="subpage drawer drawer--right drawer-close">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">
            <?php include_once "pan.php"; ?>

            <h2 class="headline3 pb pt" id="panmenu">焼きたてパンメニュー</h2>

            <?php if( have_rows('人気ランキング',$ranking_postid)): ?>
            <section class="ranking">
                <div class="wrapper">
                    <h2 class="headline1 mb">人気ランキング</h2>
                </div>
                <div class="outer bg_orange">
                    <div class="wrapper">
                        <ul class="grid_col3 cf pt_l pb">
                            <?php $rank_num = 1; ?>
                            <?php while( have_rows('人気ランキング',$ranking_postid) ): the_row(); ?>

                            <li class="col rank<?php echo $rank_num; ?>">
                                <?php $ranking_img = get_sub_field('画像'); ?>
                                <img class="img_center" src="<?php echo $ranking_img['sizes']['ranking_img'];?>" alt="<?php echo the_title(); ?>">
                                <h3 class="pt_s"><?php echo get_sub_field('タイトル'); ?></h3>
                                <p><?php echo get_sub_field('サブタイトル'); ?></p>
                            </li>

                            <?php $rank_num++; ?>
                            <?php
          if($rank_num==4) {
            break;
          }
        ?>
                            <?php endwhile; ?>

                        </ul>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- outer -->

                <div class="wrapper">
                    <h2 class="headline1 mt_l mb">ハールくんおすすめランキング</h2>
                </div>
                <div class="outer bg_img1">
                    <div class="wrapper">
                        <ul class="grid_col3 cf pt_l pb">
                            <?php $rank_num = 1; ?>
                            <?php while( have_rows('ハールくんおすすめランキング',$osusume_postid) ): the_row(); ?>

                            <li class="col rank<?php echo $rank_num; ?>">
                                <?php $ranking_img = get_sub_field('画像'); ?>
                                <img class="img_center" src="<?php echo $ranking_img['sizes']['ranking_img'];?>" alt="<?php echo the_title(); ?>">
                                <h3 class="pt_s"><?php echo get_sub_field('タイトル'); ?></h3>
                                <p><?php echo get_sub_field('サブタイトル'); ?></p>
                            </li>

                            <?php $rank_num++; ?>
                            <?php
              if($rank_num==4) {
                break;
              }
            ?>
                            <?php endwhile; ?>

                        </ul>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- outer -->

            </section>
            <!-- ranking -->
            <?php endif; ?>

            <section class="newitem">
                <div class="wrapper">
                    <h2 class="headline1 mt_l mb">季節限定の新商品</h2>
                </div>
                <div class="outer bg_gray2 pt_l pb_l">
                    <div class="wrapper">
                        <ul class="grid_col4 cf">
                            <li class="col">
                                <img class="img_center" src="./images/menu_new_photo1.jpg">
                                <h3 class="title pt_l">タイトルタイトルタイトルタイトルタイトルタイトル</h3>
                                <p class="text pt_s fsize_s">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                            </li>
                            <li class="col">
                                <img class="img_center" src="./images/menu_new_photo1.jpg">
                                <h3 class="title pt_l">タイトルタイトルタイトルタイトルタイトルタイトル</h3>
                                <p class="text pt_s fsize_s">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                            </li>
                            <li class="col">
                                <img class="img_center" src="./images/menu_new_photo1.jpg">
                                <h3 class="title pt_l">タイトルタイトルタイトルタイトルタイトルタイトル</h3>
                                <p class="text pt_s fsize_s">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                            </li>
                            <li class="col">
                                <img class="img_center" src="./images/menu_new_photo1.jpg">
                                <h3 class="title pt_l">タイトルタイトルタイトルタイトルタイトルタイトル</h3>
                                <p class="text pt_s fsize_s">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                            </li>
                        </ul>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- outer -->
            </section>
            <!-- newitem -->

            <section class="voice1">
                <div class="wrapper">
                    <div class="outer cf">
                        <div class="text pb_l pt_s">さくさくの生地に大きな牛肉がゴロ～リ！<br class="pc">店内で仕込んだ自慢のカレーパンです♪</div>
                        <div class="box">
                            <img class="img_center" src="./images/menu_voice_photo1.jpg" alt="">
                            <p class="fsize_s">カラッとさくっと、冷めても美味しい</p>
                        </div>
                    </div>
                    <!-- outer -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- voiece1 -->

            <section class="teiban">
                <div class="wrapper">
                    <h2 class="headline1">定番ラインナップ</h2>
                    <ul class="grid_col4 cf pt">
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo1.jpg">
                            <h3 class="title">十勝産つぶあんぱん</h3>
                            <p class="text">道産の小豆を使用したあんこを包みました</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo2.jpg">
                            <h3 class="title">フレンチトースト</h3>
                            <p class="text">天然酵母の角食パンにカスタードをたっぷり絞った大人気フレンチトースト</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo3.jpg">
                            <h3 class="title">パンダさん</h3>
                            <p class="text">自家製カスタードクリームとパンダクッキーが可愛い</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo4.jpg">
                            <h3 class="title">パン・オ・ショコラ</h3>
                            <p class="text">サックサクのクロワッサンにチョコレートを包みました</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo5.jpg">
                            <h3 class="title">ロングソーセージ</h3>
                            <p class="text">パリっとソーセージにピリっとマスタードが◎</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo6.jpg">
                            <h3 class="title">ラウンド北海道バター</h3>
                            <p class="text">生クリームと北海道バターモチっとした食感が特徴</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo7.jpg">
                            <h3 class="title">特製明太フランス</h3>
                            <p class="text">フランス生地に特製明太ソースを塗り焼き上げ</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo8.jpg">
                            <h3 class="title">おろしチキンバーガー</h3>
                            <p class="text">サクッと揚げたチキンにたっぷりと大根おろしをいれてサッパリとしたバーガーにしました</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo9.jpg">
                            <h3 class="title">天然酵母の角食パン</h3>
                            <p class="text">天然酵母ルヴァンを使用<br>しっとりなめらか食パン</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo10.jpg">
                            <h3 class="title">SOYマフィン</h3>
                            <p class="text">
                                おからと豆乳を使用した素材感を活かしたシンプルなマフィン。おみやげにおやつにスタッフおすすめ
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo11.jpg">
                            <h3 class="title">豆乳ブレッド</h3>
                            <p class="text">濃厚豆乳を生地にたっぷり栄養満点の食パンです</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo12.jpg">
                            <h3 class="title">ふわとろクリームパン</h3>
                            <p class="text">自家製のカスタードクリームとろけるクリームと評判です</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo13.jpg">
                            <h3 class="title">スパイスチョリソー</h3>
                            <p class="text">スパイシーなチョリソーを豪快に包んで焼きました</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo14.jpg">
                            <h3 class="title">さくさくメロンパン</h3>
                            <p class="text">ふわふわ生地にサクサクのクッキーを合わせました</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo15.jpg">
                            <h3 class="title">豆乳ロール</h3>
                            <p class="text">手作り豆乳をたっぷり使用ここだけのオリジナルメニュー</p>
                        </li>
                        <li class="col matchheight">
                            <img class="img_center" src="./images/menu_teiban_photo16.jpg">
                            <h3 class="title">石窯ピザ</h3>
                            <p class="text">地元産の野菜や食材使用<br>ご注文後、焼き上げます</p>
                        </li>
                    </ul>
                </div>
                <!-- wrapper -->
            </section>
            <!-- teiban -->

            <section class="voice2">
                <div class="wrapper">
                    <div class="outer cf">
                        <div class="box">
                            <img class="img_center" src="./images/menu_voice_photo2.jpg" alt="">
                            <p class="fsize_s">ピザはご注文いただいてからその都度焼き上げます<br>※お時間少々いただきます</p>
                        </div>
                        <div class="text pt_s">季節の食材を使用したあつあつピザ！<br class="pc">スタッフも大人気のメニューです。</div>
                    </div>
                    <!-- outer -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- voiece2 -->

            <section class="cafemenu" id="cafemenu">
                <h2 class="headline3 pb_s">カフェメニュー</h2>
                <div class="wrapper">
                    <img src="./images/menu_cafe<?php echo mobile_img(); ?>.jpg">
                    <div class="pt_l grid_col2 ptn2 outer cf">
                        <div class="col left">
                            <dl class="grid_col2 ptn2 cf mb_l">
                                <dt class="col"><img src="./images/menu_cafe_photo1.jpg" alt=""></dt>
                                <dd class="col">
                                    <h3 class="pb_s">豆乳ソフトクリーム</h3>
                                    <p>姉妹店の湧水の里豆腐工房の豆乳を使用した完全オリジナルメニューです</p>
                                </dd>
                            </dl>

                            <dl class="grid_col2 cf">
                                <dt class="col"><img src="./images/menu_cafe_photo2.jpg" alt=""></dt>
                                <dd class="col">
                                    <h3 class="pb_s">湧水コーヒー（HOT/ICE）</h3>
                                    <p>羊蹄山の湧水を使用した挽きたてコーヒー。パンと一緒にいかがですか？</p>
                                </dd>
                            </dl>
                        </div>
                        <!-- left -->

                        <div class="col right">

                            <div class="outer cf">
                                <div class="box">
                                    <img class="img_center" src="./images/menu_voice_photo3.jpg" alt="">
                                    <p class="fsize_s">天気のいい日はテラスで</p>
                                </div>
                                <div class="text pb_l pt_s">私、野菜ソムリエです&#9834;</div>
                            </div>
                            <!-- outer -->
                        </div>
                        <!-- right -->
                    </div>
                    <!-- outer -->

                </div>
            </section>
            <!-- cafemenu -->

            <section id="bottom_link">
                <div class="wrapper">
                    <ul class="grid_col3 cf pb">

                        <li class="col">
                            <a href="<?php echo $root_path; ?>kodawari.php">
                                <div class="photo photo1">
                                    <img src="./images/bottom_link_photo1.jpg">
                                    <div class="text">HARUのこだわり</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>ニセコ産の豊かな食材を、職人こだわりのレシピでお届け。</p>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>shop.php">
                                <div class="photo photo2">
                                    <img src="./images/bottom_link_photo2.jpg">
                                    <div class="text">店舗情報</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>焼き立てパンをその場で味わえるカフェ＆テラス</p>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>shop.php#access">
                                <div class="photo photo3">
                                    <img src="./images/bottom_link_photo3.jpg">
                                    <div class="text">アクセス</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>ニセコビュープラザ前。お店へのアクセスはこちら</p>
                        </li>
                    </ul>
                </div>
            </section>

        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
