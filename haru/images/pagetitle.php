<?php
$keys = parse_url($_SERVER["REQUEST_URI"]);
$str = explode("/", $keys['path']);
$url = preg_replace('/\.[^.]+$/','',end($str));
?>
<div id="pagetitle">
    <div class="photo <?php echo $url; ?>" style="background:url(./images/pagetitle_<?php echo $url; ?>.jpg) no-repeat;">
        <div class="bg">
            <h2>       
            <?php if($url=="kodawari"): ?>
                こだわり
            <?php elseif($url=="menu"): ?>
                メニュー
            <?php elseif($url=="shop"): ?>
                店舗案内
            <?php elseif($url=="contact"): ?>
                お問い合わせ
            <?php endif; ?>
            <span class="underline"></span>

            </h2>
        </div>
        <!-- bg -->
    </div>
</div>
<!-- pagetitle -->