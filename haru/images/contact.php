<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "";
$description = "";
$keyword = "";
$ranking_postid = 14;

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_contact" class="drawer drawer--right">
<div id="outer">
  <?php include_once "header.php"; ?>
  <div id="contents">

	<section class="contact bgimg1">
		<div class="wrapper">
				<h3 class="headline1">お問い合わせフォーム</h3>

						<form id="mailformpro" action="form/mailformpro/mailformpro.cgi" method="POST">

						<div class="privacy_area">
						<p class="h"><a href="http://foryou2004.com/privacy.html" target="_blank">お客様の個人情報の取り扱いについて</a></p>
						<p class="t2">本ページでご入力いただく情報は、弊社「<a href="http://foryou2004.com/privacy.html" target="_blank">プライバシーポリシー</a>」に基づき、お問い合わせに対するご連絡に使用させて頂きます。<br>
							弊社「プライバシーポリシー」をお読みいただき、ご同意いただける場合のみ、下の「プライバシーポリシーに同意する」にチェックをし、必要事項を入力のうえ、お進みください。 </p>
				
						<div class="check">
						<label>
						<input type="checkbox" name="プライバシーポリシー" value="同意します" required="required">
						プライバシーポリシーに同意する</label>
						</div>
						</div><!--privacy_area -->
						

						
						<table border="0"  class="form">
							<tr>
								<th scope="row"><em>※必須</em>お問い合わせ内容</th>
								<td>
									<label><input type="radio" name="content" value="1" checked="checked">お問い合わせ</label>
									<label><input type="radio" name="content" value="2">採用について</label>
								</td>
							</tr>
							<tr>
								<th scope="row"><em>※必須</em>お名前</th>
								<td><input type="text" name="お名前" value="" size="40" maxlength="60" required placeholder="鈴木太郎"/></td>
							</tr>
							<tr>
								<th scope="row"><em>※必須</em>メールアドレス</th>
								<td><input type="text" data-type="email" data-parent="mailfield" name="email" size="40" maxlength="60" required placeholder="abc@foryou2004.com" /><br>
		<p class="memo">※半角英数字<br>
		※自動返信メールが届きます。ページ下部をご確認下さい。</p></td>
							</tr>
							<tr>
								<th scope="row"><em>※必須</em>ご質問など</th>
								<td><textarea name="ご質問など" cols="42" rows="15" required placeholder="お問い合わせやご質問など入力"></textarea></td>
							</tr>

						</table>
						<div class="mfp_buttons">
							<button type="submit">入力内容を確認する</button>
						</div>
					<div class="privacy">
		<p>お問い合わせ送信完了後に10分以上経っても自動返信メールが届かない場合は以下の原因が考えられます。</p>
		<p><span class="bold">1.メールアドレスの記入間違いの可能性</span><br>
		メールアドレスをご記入の際は、間違いが無いか再度ご確認ください。<br>
		誤記入にお気づきになった場合は、フォームより再度お送り頂くか、大変お手数をお掛けいたしますが、ご希望店舗までお電話でお問合わせください。</p>
		<p><span class="bold">2.フリーメールのフィルタリング機能によって迷惑メールとして処理されている可能性</span><br>
		フリーメール（Yahoo、Hotmail、Gmail等）をご利用の場合、迷惑メールフィルタリング機能により、<br>
		返信メールを迷惑メール（スパムメール）として扱われている場合があります。<br>
		ご利用されているフリーメールの「迷惑メールフォルダ」や「削除フォルダ」をご確認ください。<br>
		※フリーメールの種類や設定によって、迷惑メールフォルダに入ったメールが数日後に自動削除される場合がございます。</p>
		<p><span class="bold">3.ご使用中のメールサーバー容量が一杯になっている可能性</span><br>
		メールサーバーの容量が一杯になると、メールの受信ができなくなります。メールアドレスを取得されたプロバイダやフリーメール提供元にてご確認ください。</p>
		<p><span class="bold">4.メール受信拒否設定で拒否されている可能性</span><br>
		携帯キャリアメールをご利用の場合は下記ドメインの指定解除をお願いします。<br>
		「@foryou2004.co.jp」</p>
						</div><!--privacy -->
						
						
					</form>
					<script type="text/javascript" id="mfpjs" src="form/mailformpro/mailformpro.cgi" charset="UTF-8"></script>
		</div>
		<!-- wrapper -->
	</section>
		
	</div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>