<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "HARUの店舗情報・アクセス | 【公式】石窯パンマルシェ HARU";
$description = "「羊蹄山」が一望できるロケーション。天気の良い日はテラス席がおすすめです。イートインコーナーもご用意しています。ドライブの休憩に焼きたてパンはいかがですか？道の駅ニセコビュープラザのすぐ隣です♪";
$keyword = "ハル,HARU,北海道,ニセコ,虻田郡,羊蹄山,駐車場,焼きたてパン,カフェ,スペイン石釜,豆乳,ソフトクリーム,湧水コーヒー,スイーツ,イートインスペース,テラス,絶景";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_shop" class="subpage drawer drawer--right drawer-close">

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">
            <?php include_once "pan.php"; ?>

            <section class="shokai pt_l pb_l">
                <div class="wrapper">
                    <h2 class="headline1 mb">店舗紹介</h2>
                    <h3 class="fsize_xl mb">大自然に抱かれたテラスで食べる、焼き立てパン</h3>
                    <div class="outer cf">
                        <div class="left">
                            <dl>
                                <dt><img src="./images/str_photo1.jpg" alt=""></dt>
                                <dd class="pt_s">
                                    <h4 class="pb_s">石窯パンマルシェHARU（ハル）</h4>
                                    <address>
                                        <dl class="cf">
                                            <dt>住　所：</dt>
                                            <dd><a href="https://www.google.com/maps?ll=42.798752,140.702465&z=16&t=m&hl=ja&gl=JP&mapclient=embed&cid=14055678783859758851" target="_blank">北海道虻田郡ニセコ町元町55-1道の駅ニセコビュープラザ横</a></dd>
                                        </dl>
                                        <dl class="cf">
                                            <dt>電　話：</dt>
                                            <dd x-ms-format-detection="none"><a href="tel:0136-55-6301">0136-55-6301</a></dd>
                                        </dl>
                                        <dl class="cf">
                                            <dt>時　間：</dt>
                                            <dd>夏期（4月～10月）平日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10:00～18:00
                                                <div class="right-ad1">土日祝&nbsp;&nbsp;&nbsp;&nbsp;9:00～18:00</div>
                                                <div class="right-ad2">冬期（11月～3月）全日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10:00～17:00
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="cf">
                                            <dt>定休日：</dt>
                                            <dd>年末年始</dd>
                                        </dl>

                                        <dl class="cf">
                                            <dt>駐車場：</dt>
                                            <dd>40台</dd>
                                        </dl>
                                        <dl class="cf">
                                            <dt>席　数：</dt>
                                            <dd>イートイン 24席　テラス 24席</dd>
                                        </dl>
                                    </address>
                                </dd>
                            </dl>
                        </div>
                        <!-- left -->
                        <div class="right">
                            <ul>
                                <li class="row1">
                                    <img src="./images/str_photo2.jpg">
                                    <p>快適な、イートインスペース</p>

                                </li>
                                <li class="row2 mt_l">
                                    <img src="./images/str_photo3.jpg">
                                    <p>羊蹄山を一望するテラス席</p>

                                </li>
                            </ul>
                        </div>
                        <!-- right -->
                    </div>
                    <!-- outer -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- shokai -->

            <section class="access bg_gray2 pb_l" id="access">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2927.489273548453!2d140.70026936584375!3d42.79914572915986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0aaf691486d455%3A0xc30fcd623cf49703!2z55-z56qv44OR44Oz44Oe44Or44K344KnIEhBUlU!5e0!3m2!1sja!2sjp!4v1539770549656" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div class="wrapper">
                    <h3 class="pt">アクセス</h3>
                    <h4 class="fsize">●JRニセコ駅から</h4>
                    <p>車／道道66号線で３分</p>

                    <h4 class="fsize">●比羅夫（ヒラフ）エリアから</h4>
                    <p>車／道道631号線、国道5号線経由で14分</p>

                    <h4 class="fsize">●札幌から（中山峠経由）</h4>
                    <p>車／国道230号線、道道66号線経由で2時間10分</p>

                    <h4 class="fsize">●小樽から</h4>
                    <p>車／国道5号線で1時間30分</p>

                    <h4 class="fsize">●新千歳空港から（支笏湖経由）</h4>
                    <p>車／道道16号線、国道276号線、国道230号線、道道66号線経由で2時間10分</p>
                </div>
                <!-- wrapper -->
            </section>
            <!-- access -->

            <section class="herlkun">
                <div class="wrapper">
                    <h3>こんにちは、ハールくんです！</h3>
                    <img class="img_center" src="./images/str_herlkun.png" alt="">
                    <p class="pt_s">僕の名前はハールです。石窯パンマルシェHARUに住む、シマリスです。お店のいろんなところにいるから探してみてね&#9834;</p>
                </div>
                <!-- wrapper -->

            </section>
            <!-- herlkun -->

            <section id="bottom_link">
                <div class="wrapper">
                    <ul class="grid_col3 cf pb">

                        <li class="col">
                            <a href="<?php echo $root_path; ?>kodawari.php">
                                <div class="photo photo1">
                                    <img src="./images/bottom_link_photo1.jpg">
                                    <div class="text">HARUのこだわり</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>ニセコ産の豊かな食材を、職人こだわりのレシピでお届け。</p>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>menu.php">
                                <div class="photo photo1">
                                    <img src="./images/bottom_link_photo4.jpg">
                                    <div class="text lg">焼き立てパンメニュー</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>地元食材を使用した種類豊富なメニューを紹介</p>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>menu.php#cafemenu">
                                <div class="photo photo3">
                                    <img src="./images/bottom_link_photo5.jpg">
                                    <div class="text">カフェメニュー</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                            <p>焼きたてパンと湧水コーヒーでほっと一息。ドライブの休憩にも。</p>
                        </li>
                    </ul>
                </div>
            </section>

        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
