<?php
  $url = $_SERVER['REQUEST_URI'];
  $tmp = explode("/", $url);
  $term_slug = end($tmp);
?>
<header id="top">
  <div class="pcmenu cf">
    <h1 title="石窯パンマルシェ HARU">
    <a href="<?php echo $root_path; ?>">
      <div class="header-logo">
        <div class="top"><img src="<?php echo $root_path; ?>images/header_logo.svg" alt="北海道ニセコ町 | 石窯パンマルシェ HARU | ハル"></div>
        <div class="kasou"><img src="<?php echo $root_path; ?>images/header_logo_s.svg" alt="北海道ニセコ町 | 石窯パンマルシェ HARU | ハル"></div>
      </div>
    </a></h1>
    <div class="header-outer">
      <nav>
        <ul class="cf">
          <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
          <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>menu.php#top">焼きたてパンメニュー</a></li>
          <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>menu.php#cafemenu">カフェメニュー</a></li>
          <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>shop.php">店舗情報</a></li>
          <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
        </ul>
      </nav>
      <div class="header-right cf">
        <ol>
          <?php if(is_pc()): ?>
          <li class="lang pc" id="google_translate_element">
            <script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'ja', includedLanguages: 'ja,en,fr,ko,ms,ru,th,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
              }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <!-- google翻訳後にレイアウト（高さ）がくずれるコンテンツをそろえる　-->
            <div id="translationDetector" style="display:none;">English</div>
            <script type="text/javascript">
                var origValue = document.getElementById("translationDetector").innerHTML;
                document.getElementById("translationDetector").addEventListener("DOMSubtreeModified", translationCallback, false);

                    function translationCallback() {
                    var currentValue = document.getElementById("translationDetector").innerHTML;
                    if (currentValue && currentValue.indexOf(origValue) < 0) {
                        origValue = currentValue;
                        setTimeout(function(){

                            if(isGoogtransJa()) {
                                $(".googtrans-box").css("display","flex")
                            } else {
                                $(".googtrans-box").css("display","block")
                            }

                            $('.sub-matchheight').matchHeight();
                            $('.matchHeight').matchHeight();
                        },1000);
                    }
                }
            </script>
          </li>
          <?php endif; ?>
          <li><a href="https://www.facebook.com/marche.haru/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png" alt="Facebook"></a></li>
          <li><a href="https://www.instagram.com/marche.haru/" target="_blank" alt="Instagram"><img src="<?php echo $root_path; ?>images/header_instagram.png" alt="Instagram"></a></li>
        </ol>
      </div>
      <!-- header-right -->
    </div>
    <!-- header-outer -->
  </div>
  <!-- pcmenu -->

  <div class="spmenu drawermenu" role="banner" id="top">
    <h2 class="cf"><a href="<?php echo $root_path; ?>"><img src="<?php echo $root_path; ?>images/header_logo_s.svg" alt="石窯パンマルシェ HARU"><span class="text"><img src="<?php echo $root_path; ?>images/header_text_sp.svg" alt="石窯パンマルシェ HARU"></span></a></h2>
    <button type="button" class="drawer-toggle drawer-hamburger"> <span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span>
      <img class="menu-icon" src="<?php echo $root_path; ?>images/header_menu.svg" >
      </button>

    <nav class="drawer-nav" role="navigation">
      <div class="inner">
        <ul class="drawer-menu">
          <li class="top cf">
          <a class="facebook" href="https://www.facebook.com/marche.haru/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>
          <a class="instagram" href="https://www.instagram.com/marche.haru/" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a>
          <?php if(is_mobile()): ?>

          <div class="lang sp" id="google_translate_element">
            <script type="text/javascript">
              function googleTranslateElementInit() {
                new google.translate.TranslateElement({pageLanguage: 'ja', includedLanguages: 'ja,en,fr,ko,ms,ru,th,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
              }
            </script>
            <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <!-- google翻訳後にレイアウト（高さ）がくずれるコンテンツをそろえる　-->
            <div id="translationDetector" style="display:none;">English</div>
            <script type="text/javascript">
                var origValue = document.getElementById("translationDetector").innerHTML;
                document.getElementById("translationDetector").addEventListener("DOMSubtreeModified", translationCallback, false);

                function translationCallback() {
                    var currentValue = document.getElementById("translationDetector").innerHTML;
                    if (currentValue && currentValue.indexOf(origValue) < 0) {
                        origValue = currentValue;
                        setTimeout(function(){
                        $('.matchHeight').matchHeight();
                        },3000);
                    }
                }
            </script>
          </div>
          <?php endif; ?>
          </li>

          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>index.php">トップページ</a></li>
          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>menu.php">焼きたてパンメニュー</a></li>
          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>menu.php#cafemenu">カフェメニュー</a></li>
          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>shop.php">店舗情報</a></li>
          <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
<!--
          <li class="text"><a class="drawer-menu-item" href="#">採用情報</a></li>
          <li class="text"><a class="drawer-menu-item" href="#">会社情報</a></li>-->
          <li class="text"><a class="drawer-menu-item" href="#">プライバシーポリシー</a></li>
        </ul>
        <div class="store_info">
            <img src="<?php echo $root_path; ?>images/spmenu_store.jpg">
            <h3>石窯パンマルシェ HARU</h3>
            <p class="ad"><a href="https://www.google.com/maps?ll=42.798752,140.702465&z=16&t=m&hl=ja&gl=JP&mapclient=embed&cid=14055678783859758851" target="_blank">北海道虻田郡ニセコ町元町55-1道の駅ニセコビュープラザ横</a></p>
            <p x-ms-format-detection="none"><a href="tel:0136-55-6301">0136-55-6301</a></p>
        </div>
        <!-- store_info -->

      </div>
      <!--inner -->
    </nav>
  </div>
  <!-- spmenu -->
</header>
<main role="main">
