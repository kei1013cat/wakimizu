<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "送信完了 | 石窯パンマルシェ HARU";
$description = "ニセコの道の駅、ビュープラザの向かいにある石窯パンマルシェHARU(ハル)のお問い合わせページです。お問い合わせやご意見はこちらのフォームからお気軽にお問い合わせください。";
$keyword = "ハル,HARU,北海道,ニセコ,虻田郡,羊蹄山,駐車場,焼きたてパン,カフェ,スペイン石釜,豆乳,ソフトクリーム,湧水コーヒー,スイーツ,イートインスペース,テラス,絶景";
$ranking_postid = 14;

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_thanks" class="subpage drawer drawer--right">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
<?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">
  <?php include_once "pan.php"; ?>

	<section class="contact">
		<div class="wrapper">
			<h3 class="headline1 mt">送信完了</h3>
			<div class="box mb_l">
				<p>この度はお問い合せ頂き誠にありがとうございました。改めて担当者よりご連絡をさせていただきます。</p>
				<p class="linkbtn1 mt ">
					<a href="./index.php">TOPページへ戻る</a>
				</p>
			</div>
		</div>
		<!-- wrapper -->
	</section>
		
	</div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

<script type="text/javascript" src="form/mfp.statics/thanks.js" charset="UTF-8"></script>


</body>
</html>