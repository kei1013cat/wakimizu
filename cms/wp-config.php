<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'almacheck_test_wakimizu');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'almacheck');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'efe3t83juhy3');

/** MySQL のホスト名 */
define('DB_HOST', 'mysql633.db.sakura.ne.jp');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6~NC4b`;h4~63%[nCc/{cW[ZOLZf< wM gaIi4FgBJ*EK 1K6GVF7ONBEIf:@IZj');
define('SECURE_AUTH_KEY',  'XY:%a<rfB^uy7EV(w-JKxTf)sC:m6_tJTR^}?%}vtaXm.ojH{mIaC(h|wafJ}aE;');
define('LOGGED_IN_KEY',    'qeCaZryfT9>*cSlS8k/3~$Q&La2<XCc9Ezr&)N94*,80VDK`HY<4F0_;*Yf}G~E^');
define('NONCE_KEY',        '!xywp_^Taw5qd]mSDJO7f6y} zKzXgLpm5,&j&F&]K/a;Sc7o1fVP~-szm2wpGH/');
define('AUTH_SALT',        'YJ^{;AZnOGMsl;ODeO]kK0t0Yo~%ekdZ`:=JEV[=hu4By1M*?Mc_Cq2aLHie> {e');
define('SECURE_AUTH_SALT', 'RPe*F4xGn).:V#0~*z}5]B;s>v,j}+{}+,O1@:s2eWp8WIVJW)Tcg#]P%Nt@&}PE');
define('LOGGED_IN_SALT',   '*{UTtN%4jz*E0.DfG8,]:o^zJa?4&WZ$.uh=&Z-@hxwe3)B,yr+03*m8`/s.!+oN');
define('NONCE_SALT',       ':gs)GB+QNx-i^9~}A#v(h6=Di:O,~DX$TYtoo2u1u{;7&$V|/#@@IDn&pG0CE`NC');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
