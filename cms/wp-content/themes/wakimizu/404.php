<?php get_header(); ?>

<div id="title">
	<div class="wrapper">
		<?php $slug_name = basename(get_permalink()); ?>
		<h2><?=$post->post_title;?></h2>
	</div>
</div>
<!-- title -->

	<div id="main_contents" class="cf">
		<div id="contents" class="mr15">
			<section class="coming-soon">
				<div class="bg">
				<h2>COMING SOON</h2>
				<a href="<?php bloginfo('url'); ?>">トップページへ戻る</a>
				</div>
			</section>
		</div>
		<!-- contents -->

	</div>
	<!-- main_contents --> 

<?php get_footer(); ?>
