<?php
$category = get_the_category();
$cat_id   = $category[0]->cat_ID;
$cat_name = $category[0]->cat_name;
$cat_slug = $category[0]->category_nicename;
$root_path = "../../../".$cat_slug ."/";
$title = "";
$description = "";
$keyword = "";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "../".$cat_slug."/head.php"; ?>
</head>

<body id="page_category" class="subpage drawer drawer--right">
<div id="outer">
<?php include_once "../".$cat_slug."/header.php"; ?>
	<div id="pagetitle">
		<div class="photo topics">
			<div class="bg">
				<h2>お知らせ一覧<span class="underline"></span></h2>
			</div>
			<!-- bg -->
		</div>
	</div>
	<!-- pagetitle -->

<div id="contents">

<div id="pan">
	<div class="wrapper">
	<a href="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/index.php">農家のそばや 羊蹄山</a>&nbsp;&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;&nbsp;お知らせ一覧
	</div>
	<!-- wrapper -->
</div>
<!-- pan -->

    <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '-1', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC',
      'category_name' => $cat_slug // 表示したいカテゴリーのスラッグを指定
    );
    $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>

    <section class="topics pt_l pb_l">
      <div class="wrapper pb">
	    <h3 class="headline4 mt mb">お知らせ一覧</h3>

        <?php while($wp_query->have_posts()) :?>
        <?php $wp_query->the_post(); ?>

        <a href="<?php the_permalink() ?>">
        <dl class="cf">
          <dt><?php the_time('Y.m.d'); ?></dt>
          <dd><?php echo $post->post_title; ?></dd>
        </dl>
        </a>

        <?php endwhile; ?>

        <?php endif; ?>
        <?php wp_reset_query(); ?>

      </div>
      <!-- wrapper -->
    </section>
    <!-- topics -->



</div>

<?php include_once "../".$cat_slug."/footer.php"; ?>
