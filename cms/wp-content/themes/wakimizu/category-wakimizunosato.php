<?php
$category = get_the_category();
$cat_id   = $category[0]->cat_ID;
$cat_name = $category[0]->cat_name;
$cat_slug = $category[0]->category_nicename;
$root_path = "../../../".$cat_slug ."/";
$title = "トピックス |【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "../".$cat_slug."/head.php"; ?>
</head>

<body id="page_category" class="subpage drawer drawer--right drawer-close">
<div id="outer">
<?php include_once "../".$cat_slug."/header.php"; ?>
	<div id="pagetitle">
		<div class="photo topics">
            <div class="wrapper">
                <h2>トピックス</h2>
            </div>
		</div>
	</div>
	<!-- pagetitle -->

<div id="contents">

    <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '-1', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC',
      'category_name' => $cat_slug // 表示したいカテゴリーのスラッグを指定
    );
    $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>

    <section class="topics pt_l pb_l">
      <div class="wrapper">
        <?php while($wp_query->have_posts()) :?>
        <?php $wp_query->the_post(); ?>

        <dl class="cf">
          <dt>
            <p class="date fsize_s"><?php the_time('Y-m-d'); ?></p>
          </dt>
          <a href="<?php the_permalink() ?>">
          <dd>
            <h3 class="fsize_l"><?php echo $post->post_title; ?></h3>
          </dd>
          </a>
        </dl>
        <?php endwhile; ?>

        <?php endif; ?>
        <?php wp_reset_query(); ?>

    <section class="net_store pt_l mt pb">
        <p><a href="<?php bloginfo('url'); ?>/../eccube/html/"><img src="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
    </section>
    <!-- net_store -->

      </div>
      <!-- wrapper -->
    </section>
    <!-- topics -->


</div>

<?php include_once "../".$cat_slug."/footer.php"; ?>
