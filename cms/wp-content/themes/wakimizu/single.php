<?php
$category = get_the_category();
$cat_id   = $category[0]->cat_ID;
$cat_name = $category[0]->cat_name;
$cat_slug = $category[0]->category_nicename;

$root_path = "../../".$cat_slug."/";

if ( have_posts() ) : while ( have_posts() ) : the_post();
    $title = get_the_title();
    $content = get_the_content();
    $content = wp_strip_all_tags( $content );
    endwhile; endif;
wp_reset_query();

if ($cat_slug == "haru") {
    $title_comp = "【公式】石窯パンマルシェ HARU";
} elseif($cat_slug == "yoteizan") {
    $title_comp = "【公式】農家のそばや 羊蹄山";
} elseif($cat_slug == "wakimizunosato") {
    $title_comp = "【公式】真狩豆腐工房 湧水の里";
}

$title = $title." ｜ ".$title_comp;
$description = $content;
$keyword = "";
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "../".$cat_slug."/head.php"; ?>
</head>

<body id="page_single" class="subpage drawer drawer--right drawer-close">
<div id="outer">
<?php include_once "../".$cat_slug."/header.php"; ?>

    <?php if($cat_slug == "wakimizunosato"): ?>
        <div id="pagetitle">
            <div class="photo topics">
                <div class="wrapper">
                    <h2>トピックス</h2>
                </div>
            </div>
        </div>
        <!-- pagetitle -->
    <?php else: ?>
        <div id="pagetitle">
            <div class="photo topics">
                <div class="bg">
                    <h2>お知らせ<span class="underline"></span></h2>
                </div>
                <!-- bg -->
            </div>
        </div>
        <!-- pagetitle -->
    <?php endif; ?>

<div id="contents">

<?php if($cat_slug != "wakimizunosato"): ?>
<div id="pan">
	<div class="wrapper">
	<a href="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/index.php"><?php if( $cat_slug == "yoteizan"):?>農家のそばや 羊蹄山<?php else:?>TOP<?php endif; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;&nbsp;お知らせ
	</div>
	<!-- wrapper -->
</div>
<!-- pan -->
<?php endif; ?>

<section class="news">
	<div class="box wrapper">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article <?php post_class(); ?>>
        
        <?php if ($cat_slug == "haru"): ?>
            <div class="entry-header">
                <div class="news-outer cf">
                    <div class="photo">
                        <?php the_post_thumbnail('thumbnail'); ?>
                    </div>
                    <!-- photo -->
                    <div class="text">
                        <p>
                            <time class="entry-date fsize_s" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
                                <?php the_time( 'Y.m.d'  ); ?>
                            </time>
                        </p>
                        <h3 class="entry-title">
                            <?php the_title(); ?>
                        </h3>
                    </div>
                    <!-- text -->
                </div>
                <!-- news-outer -->
            </div>
        <?php else: ?>
            <div class="entry-header">
                <p>
                    <time class="entry-date fsize_s" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
                        <?php if ($cat_slug == "wakimizunosato"): ?>
                            <?php the_time( 'Y-m-d'  ); ?>
                        <?php else: ?>
                            <?php the_time( 'Y.m.d'  ); ?>
                        <?php endif; ?>
                    </time>
                </p>
                <h3 class="entry-title">
                    <?php the_title(); ?>
                </h3>
            </div>
        <?php endif; ?>
        
		<section class="entry-content cf">
        <?php if ($cat_slug == "haru"): ?>
            <div class="left">
                <?php if(has_post_thumbnail()): ?>
                    <?php the_post_thumbnail('medium'); ?>
                <?php else: ?>
                    <img src="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/images/noimg.jpg" alt="no image">
                <?php endif; ?>
            </div>
            <!-- left -->
            <div class="right">
                <?php the_content(); ?>
            </div>
            <!-- right -->
        <?php elseif ($cat_slug == "wakimizunosato"): ?>
            <div class="left">
                <?php the_content(); ?>
            </div>
            <!-- left -->
            <div class="right">
                <?php if(has_post_thumbnail()): ?>
                    <?php the_post_thumbnail('topics_wakimizusato'); ?>
                <?php else: ?>
                    <img src="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/images/noimage.png" alt="no image">
                <?php endif; ?>
            </div>
            <!-- right -->
        <?php else: ?>
			<?php the_content(); ?>
        <?php endif; ?>
		</section>
		<ul class="page_link cf">
			<li class="prev">
				<?php previous_post_link('%link', '« 前の記事へ', true); ?>
			</li>
			<li class="next">
				<?php next_post_link('%link', '次の記事へ »', true); ?>
			</li>
		</ul>
	</article>
	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>
	</div>
</section>

<?php if($cat_slug == "wakimizunosato"): ?>
    <p class="linkbtn"><a href="<?php bloginfo('url'); ?>/category/wakimizunosato/">トピックス一覧へ戻る</a></p>
    <section class="net_store pt mt pb mb_l">
        <div class="wrapper">
        <p><a href="<?php bloginfo('url'); ?>/../eccube/html/"><img src="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
        </div>
        <!-- wrapper -->
    </section>
    <!-- net_store -->
<?php else: ?>
    <p class="linkbtn1 mt_l mb_l"><a href="<?php bloginfo('url'); ?>/../<?php echo $cat_slug; ?>/index.php">トップに戻る</a></p>
<?php endif; ?>

</div>

<?php include_once "../".$cat_slug."/footer.php"; ?>
