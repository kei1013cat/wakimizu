<?
//--------------------------------------------------------------------------------------------
// エディター関連
//--------------------------------------------------------------------------------------------

//-------------------------------------------
// 使用しないメニューを非表示にする
//-------------------------------------------
function remove_admin_menus() {

    // level10以外のユーザーの場合
//    if (!current_user_can('level_10')) {
        global $menu;

        // unsetで非表示にするメニューを指定
//        unset($menu[2]);        // ダッシュボード
//        unset($menu[5]);        // 投稿
//        unset($menu[10]);       // メディア
//        unset($menu[20]);       // 固定ページ
        unset($menu[25]);       // コメント
//        unset($menu[60]);       // 外観
//        unset($menu[65]);       // プラグイン
//        unset($menu[70]);       // ユーザー
//        unset($menu[75]);       // ツール
//        unset($menu[80]);       // 設定
//    }
}
add_action('admin_menu', 'remove_admin_menus');


//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function imagepassshort($arg) {
$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
return $content;
}
add_action('the_content', 'imagepassshort');


//-------------------------------------------
//　投稿の自動整形を無効
//-------------------------------------------
//出力側の変換機能を無効化
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_content', 'wptexturize' );

// ビジュアルエディタの変換も無効
function override_mce_options( $init_array ) {
    global $allowedposttags;

    $init_array['valid_elements']          = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    $init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
    $init_array['indent']                  = true;
    $init_array['wpautop']                 = false;
    $init_array['force_p_newlines']        = false;

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );

//-------------------------------------------
// エディタをビジュアルにする
//-------------------------------------------
//add_filter('wp_default_editor', create_function('', 'return "tinymce";'));


//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------

function custom_editor_settings( $initArray ){
$initArray['block_formats'] = "段落=p; 見出し1=h3; 見出し2=h4;";
return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );


//-------------------------------------------
//投稿部分表示カスタマイズ
//-------------------------------------------
function change_post_menu_label() {
  global $menu;
  global $submenu;
  $menu[5][0] = 'お知らせ';
  $submenu['edit.php'][5][0] = 'お知らせ一覧';
  $submenu['edit.php'][10][0] = '新規作成';
  //$submenu['edit.php'][16][0] = 'タグ';
  //echo ”;
}
function change_post_object_label() {
  global $wp_post_types;
  $labels = &$wp_post_types['post']->labels;
  $labels->name = 'お知らせ';
  $labels->singular_name = 'お知らせ';
  $labels->add_new = _x('新規作成', '新着情報');
  $labels->add_new_item = '新しい情報';
  $labels->edit_item = '情報の編集';
  $labels->new_item = '新しい情報';
  $labels->view_item = '新着情報を表示';
  $labels->search_items = '新着情報検索';
  $labels->not_found = '新着情報が見つかりませんでした';
  $labels->not_found_in_trash = 'ゴミ箱の新着情報にも見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

//-------------------------------------------
//【HARU】人気ランキング
//-------------------------------------------
add_action('init', 'my_custom_init1');
function my_custom_init1()
{
  $labels = array(
    'name' => _x('【HARU】人気ランキング', 'post type general name'),
    'singular_name' => _x('【HARU】人気ランキング', 'post type singular name'),
    'add_new' => _x('新規作成', 'ranking'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => true
  );

  //カスタムタクソノミー、カテゴリタイプ
  register_taxonomy(
    'ranking-cat',
    'ranking',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );

  register_post_type('ranking',$args);
}

//-------------------------------------------
//【HARU】ハールくんおすすめランキング
//-------------------------------------------
add_action('init', 'my_custom_init2');
function my_custom_init2()
{
  $labels = array(
    'name' => _x('【HARU】ハールくんおすすめランキング', 'post type general name'),
    'singular_name' => _x('【HARU】ハールくんおすすめランキング', 'post type singular name'),
    'add_new' => _x('新規作成', 'osusume'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => true
  );

  //カスタムタクソノミー、カテゴリタイプ
  register_taxonomy(
    'osusume-cat',
    'osusume',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );

  register_post_type('osusume',$args);
}

//-------------------------------------------
//【湧水の里】人気ランキング
//-------------------------------------------
add_action('init', 'my_custom_init3');
function my_custom_init3()
{
  $labels = array(
    'name' => _x('【湧水の里】人気ランキング', 'post type general name'),
    'singular_name' => _x('【湧水の里】人気ランキング', 'post type singular name'),
    'add_new' => _x('新規作成', 'wakimizu_ranking'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい記事'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'has_archive' => true
  );

  //カスタムタクソノミー、カテゴリタイプ
  register_taxonomy(
    'wakimizu_ranking-cat',
    'wakimizu_ranking',
    array(
      'hierarchical' => true,
      'update_count_callback' => '_update_post_term_count',
      'label' => 'カテゴリー',
      'singular_label' => 'カテゴリー',
      'public' => true,
      'show_ui' => true
    )
  );

  register_post_type('wakimizu_ranking',$args);
}


//-------------------------------------------
//サムネイルサイズを追加
//-------------------------------------------

function thumbAdd() {
add_theme_support( 'post-thumbnails' ); //テーマをサムネイル表示に対応させる
add_image_size( 'ranking_img', 250,250);
add_image_size( 'topics_img', 800,800);
add_image_size( 'topics_wakimizusato', 790,530);
}
add_action( 'after_setup_theme', 'thumbAdd' );

//--------------------------------------------------------------------------------------------
// frontend
//--------------------------------------------------------------------------------------------

//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
     $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
    global $is_mobile;
    return $is_mobile;
    //return false;
}
function is_pc(){
    if(is_mobile()){
        return false;
    }else{
        return true;
    }
}
function mobile_img(){
    if (is_mobile()) {
        echo "_sp";
    }
}


//-------------------------------------------
//ページャー
//-------------------------------------------
function bmPageNaviGallery() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;

  $paginate_base = get_pagenum_link(1);
  if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
    $paginate_format = '';
    $paginate_base = add_query_arg('page', '%#%');
  } else {
    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '') .
    untrailingslashit('page/%#%', 'paged');
    $paginate_base .= '%_%';
  }
  $cat = "";
  if(!empty($_GET['cat'])){
    $cat = "&cat=".htmlspecialchars($_GET['cat'], ENT_QUOTES);
  }
  //$paginate_base = "./page/%#%".$cat;
  $result = paginate_links( array(
   // 'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 4,
    'prev_text' => '&lt;&lt;',
    'next_text' => '&gt;&gt;',
    'current' => ($paged ? $paged : 1),
  ));

  return $result;
}


add_action( 'admin_print_footer_scripts', 'limit_category_select' );
function limit_category_select() {
  ?>
<script type="text/javascript">
    jQuery(function($) {
        // 投稿画面のカテゴリー選択を制限
        var cat_checklist = $('.categorychecklist input[type=checkbox]');
        cat_checklist.click(function() {
            $(this).parents('.categorychecklist').find('input[type=checkbox]').attr('checked', false);
            $(this).attr('checked', true);
        });

        // クイック編集のカテゴリー選択を制限
        var quickedit_cat_checklist = $('.cat-checklist input[type=checkbox]');
        quickedit_cat_checklist.click(function() {
            $(this).parents('.cat-checklist').find('input[type=checkbox]').attr('checked', false);
            $(this).attr('checked', true);
        });

        $('.categorychecklist>li:first-child, .cat-checklist>li:first-child').before('<p style="padding-top:5px;">カテゴリーは1つしか選択できません</p>');
    });

</script>
<?php
}


// 「続きを読む」をカスタマイズするためのコード
function my_excerpt_more($post) {
    return  '... <span class="tuduki-underline">続きを読む</span>';
}

function my_trim_all_excerpt( $text = '' , $cut = 80 ) {
    $raw_excerpt = $text;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
        $text = strip_tags($text);

    }
    $excerpt_mblength = apply_filters('excerpt_mblength', $cut );
    $excerpt_more = my_excerpt_more( $post );
    $text = wp_trim_words( $text, $excerpt_mblength, $excerpt_more );

    return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'my_trim_all_excerpt' );


?>
