<footer>
    <div class="fotter_top bg_blue1 pt_l pb_l">
        <div class="wrapper">
            <ul class="grid_col2 ptn2 cf">
                <li class="col">
                    <div class="outer cf">
                        <div class="left">
                            <a href="#"><img src="<?php echo $root_path; ?>images/footer_logo.svg"></a>
                        </div>
                        <div class="right pt_s">
                            <h3>農家のそばや 羊蹄山</h3>
                        </div>
                    </div>
                    <!-- outer -->
                    <address class="pt_s">

                        <dl class="cf">
                            <dt>住　　所：</dt>
                            <dd>〒044-0075 <br class="sp">北海道虻田郡倶知安町字富士見463-1 (<a href="https://goo.gl/maps/qTxMQxgEA1P2" target="_blank">MAP</a>)</dd>
                        </dl>
                        <dl class="cf">
                            <dt>電話番号：</dt>
                            <dd x-ms-format-detection="none"><a href="tel:0136-21-2308">0136-21-2308</a>　<span class="small">※予約不可</span></dd>
                        </dl>
                        <dl class="cf">
                            <dt>営業時間：</dt>
                            <dd>【夏期（4～10月）】11：00～15：30<br>【冬期（11～3月）】11：00～14：30<br>※売り切れ次第終了する場合がございます。</dd>
                        </dl>
                        <dl class="cf">
                            <dt>定&nbsp;&nbsp;休&nbsp;&nbsp;日：</dt>
                            <dd>【夏期（4～10月）】水曜日<br>【冬期（11～3月）】水曜日・木曜日</dd>
                        </dl>
                        <p><a class="facebook" href="https://facebook.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>&nbsp;
                            <a class="instagram" href="https://www.instagram.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a></p>

                    </address>




                </li>
                <!-- col -->
                <li class="col">
                    <h3 class="headline3 pb_s">お近くにお越しの際はぜひお立ち寄りください</h3>
                    <ol class="grid_col2 ptn2 cf pt_s">
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../wakimizunosato/" target="_blank">
                                <img class="sp" src="<?php echo $root_path; ?>images/footer_str_wakimizu_sp.jpg">
                                <img class="pc" src="<?php echo $root_path; ?>images/footer_str_wakimizu.jpg">
                                <p>おいしい豆腐づくりを追求しています。お土産にも最適です。</p>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../haru/" target="_blank">
                                <img class="sp" src="<?php echo $root_path; ?>images/footer_str_haru_sp.jpg">
                                <img class="pc" src="<?php echo $root_path; ?>images/footer_str_haru.jpg">
                                <p>地元の食材を使ったパンを揃えています。併設のカフェで休憩もできます。</p>
                            </a>
                        </li>
                    </ol>

                </li>
                <!-- col -->
            </ul>
        </div>
        <!-- wrapper -->
    </div>
    <!-- footer-top -->
    <div class="fotter-bottom">
        <div class="wrapper">
            <nav>
                <ul class="cf">
                    <li class="sp"><a href="<?php echo $root_path; ?>index.php">トップページ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>menu.php">お品書き</a></li>
                    <li class="sp"><a href="http://www.youteizan.com/cgi-bin/eshop/e_shop.cgi?bunrui=all&keyword=&superkey=1&FF=0" target="_blank">オンラインショップ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>shop.php">店舗案内</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>index.php#access">アクセス</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
                <ul class="comp cf">
<!--
                    <li class="text"><a href="#">採用情報</a></li>
                    <li class="text"><a href="#">会社情報</a></li>-->
                    <li class="text"><a href="#">プライバシーポリシー</a></li>
                </ul>
                　
            </nav>
            <p class="copy">&copy; WAKIMIZUNOSATO Co., Ltd. All Right Reserved.</p>
        </div>
    </div>
    <!-- footer-bottom -->
</footer>

<div id="page_top">

    <a href="#"><img src="<?php echo $root_path; ?>images/top.svg" alt="pagetop"></a>
</div>

<script type="text/javascript" src="<?php echo $root_path; ?>js/top.js"></script>

</main>
