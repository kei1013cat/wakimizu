<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "そばのこだわり｜【公式】農家のそばや 羊蹄山";
$description = "お客様に最高に美味しい本物のそばを味わっていただきたいという思いから、「守破離（しゅはり）」の精神で日々蕎麦に向き合い、自家栽培したそばを毎日挽きたて・打ち立て・茹で立てでご提供しています。";
$keyword = "北海道,倶知安,蕎麦,ぼたんそば,コロッケ,ようてい,農家のそばや,羊蹄山,手打ちそば,十割そば,二八そば,冷そば,温そば,ビール,酒,ネットストア";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_kodawari" class="subpage drawer drawer--right drawer-close">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --> 

<div id="outer">
  <?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">
  <?php include_once "pan.php"; ?>

    <section class="kodawari_top pt_l pb_l">
      <div class="wrapper">
        <h2 class="headline4 mb_s">そばのこだわり</h2>
        <img class="pt_s" src="./images/kodawari_photo<?php mobile_img(); ?>.jpg">
        <div class="bottom">
          <h3 class="headline2 pt">私たちが大事にしているもの<br>「守破離」</h3>
          <p class="pt">先代から店舗を引き継いだ私たちは「守破離（しゅはり）」の精神で日々蕎麦に向き合っています。守破離とは、日本の伝統的な「道」において修行における段階を示したもの。<br>「守」は師や流派の教えを忠実に守り、確実に身につける段階。「破」は他から良いものを取り入れ、心技を発展させる段階。「離」は、独自の新しいものを生み出し確立させる段階。<br>私たちは、未だ「守」の段階ではありますが、先代の想いを胸に、私たちだからできる発展を目指して日々精進して参ります。</p>
        </div>
      </div>
      <!-- wrapper -->
    </section>
    <!-- kodawari_top -->

    <section class="kodawari_bottom pt_l mb_l">
      <div class="wrapper">

        <div class="flexcol photo_left cf row1">
          <div class="photo" style="background:url(./images/kodawari_photo1<?php mobile_img(); ?>.png) no-repeat center top;">
          </div>
          <!-- photo -->
          <div class="text">
            <h4 class="headline2 pb">雄大な羊蹄山の伏流水</h4>
            <p class="pt_s">雪や雨がもたらす水が、岩、木、土にしみこみ、長い時間をかけ濾過され、羊蹄山の伏流水として敷地内に出水しております。<br>山の恵みに感謝し、天然の伏流水を使用させていただいた名水蕎麦をぜひご賞味ください。</p>
          </div>
          <!-- text -->
        </div>
        <!-- flexcol -->

        <div class="flexcol photo_left cf row2 content_up">
          <div class="photo" style="background:url(./images/kodawari_photo2<?php mobile_img(); ?>.png) no-repeat center top;">
          </div>
          <!-- photo -->
          <div class="text">
            <h4 class="headline2 pb">先代が今なお育てる自家栽培</h4>
            <p class="pt_s">当店で使用するそばは、敷地内にて先代が丹精込めて今もなお自家栽培しております。その土地で育ったものを、その土地で食べていただく。<br>先代が大切に育ててきた想いを引き継ぐべく、可能な限り地産地消を心がけていきます。</p>
          </div>
          <!-- text -->
        </div>
        <!-- flexcol -->

        <div class="flexcol photo_left cf row3 content_up">
          <div class="photo" style="background:url(./images/kodawari_photo3<?php mobile_img(); ?>.png) no-repeat center top;">
          </div>
          <!-- photo -->
          <div class="text">
            <h4 class="headline2 pb">幻の品種「ぼたんそば」</h4>
            <p class="pt_s">流通量が少なく幻の品種と評される「ぼたんそば」。背が高く風で倒れやすいなど、栽培に時間と手間をかける必要があるのもその理由の一つ。<br>味わえるお店も決して多くはない、ぼたんそば特有の味と香りをお楽しみください。</p>
          </div>
          <!-- text -->
        </div>
        <!-- flexcol -->

        <div class="flexcol photo_left cf row4 content_up">
          <div class="photo" style="background:url(./images/kodawari_photo4<?php mobile_img(); ?>.png) no-repeat center top;">
          </div>
          <!-- photo -->
          <div class="text">
            <h4 class="headline2 pb">毎日挽きたて・打ち立て・<br>茹で立て</h4>
            <p class="pt_s">自家栽培のそばを毎日「挽きたて」「打ち立て」「茹で立て」で提供しております。石臼を用いた石挽きならではの風味をぜひお楽しみください。<br>十割そばと、二八そばの挽き方それぞれ変えて（殻の有無など）提供しております。</p>
          </div>
          <!-- text -->
        </div>
        <!-- flexcol -->

        <div class="flexcol photo_left cf row5 content_up">
          <div class="photo" style="background:url(./images/kodawari_photo5<?php mobile_img(); ?>.png) no-repeat center top;">
          </div>
          <!-- photo -->
          <div class="text">
            <h4 class="headline2 pb">こだわりぬいた出汁</h4>
            <p class="pt_s">当店の出汁は、鹿児島で一本釣りされた鰹で作られた「本枯節」と「かめ節」の二種を使用しています。節はその日に必要な分のみ削る「削り立て」。<br>そばの質に負けないよう、出汁にもしっかりとこだわって参ります。</p>
          </div>
          <!-- text -->
        </div>
        <!-- flexcol -->


      </div>
    </section>
    <!-- wrapper -->

    <section class="net_store pt pb_l mb_l">
      <a href="http://www.youteizan.com/cgi-bin/eshop/e_shop.cgi?bunrui=all&keyword=&superkey=1&FF=0" target="_blank"><img src="./images/btn_netstore.jpg" alt="農家のそば　羊蹄山　オリジナル商品が、買える！ | 湧水の里 ネットストア"></a>
    </section>
    <!-- net_store -->

    <section id="bottom_link">
    <div class="wrapper">
      <ul class="grid_col3 cf pb">

        <li class="col">
            <a href="<?php echo $root_path; ?>menu.php">
                <div class="photo photo1">
                    <img src="./images/bottom_link_photo1.jpg">
                    <div class="text">お品書き</div>
                    <div class="bg"></div>
                </div>
            </a>
        </li>
        <li class="col">
            <a href="<?php echo $root_path; ?>index.php#access">
                <div class="photo photo2">
                    <img src="./images/bottom_link_photo2.jpg">
                    <div class="text">アクセス</div>
                    <div class="bg"></div>
                </div>
            </a>
        </li>
        <li class="col">
            <a href="<?php echo $root_path; ?>shop.php">
                <div class="photo photo3">
                    <img src="./images/bottom_link_photo3.jpg">
                    <div class="text">店舗案内</div>
                    <div class="bg"></div>
                </div>
            </a>
        </li>
      </ul>
    </div>
    </section>
    <!-- bottom_link -->

  </div>
  <!-- contents -->

  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>