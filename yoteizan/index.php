<?php require('../cms/wp-load.php'); ?>
<?php
$root_path = "./";
$title = "【公式】農家のそばや 羊蹄山";
$description = "羊蹄山の湧き水で仕上げる、幻の牡丹そばが自慢。自家栽培・自家製粉にこだわった手打ちそばをご提供。、最高の味わいを楽しめるお蕎麦を、大自然に抱かれた環境でご堪能ください。";
$keyword = "北海道,倶知安,蕎麦,ぼたんそば,コロッケ,ようてい,農家のそばや,羊蹄山,手打ちそば,十割そば,二八そば,冷そば,温そば,ビール,酒,ネットストア";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
    <?php if(is_pc()): ?>
    <script type="text/javascript" src="<?php echo $root_path; ?>js/header_parallax.js"></script>
    <?php endif; ?>
</head>

<body id="page_index" class="drawer drawer--right drawer-close">


    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


    <div id="outer">
        <?php include_once "header.php"; ?>
        <div id="contents">

            <section id="mainvisual" class="mainimg">
                <div class="vegas-pager">
                </div>
            </section>
            <!-- mainimg -->

            <section class="about pt_l pb bg_blue1">
                <div class="wrapper">
                    <h2 class="headline1 pb">農家のそばや　羊蹄山</h2>
                    <p class="mb font_hannari">初代が、この羊蹄の麓に鍬を下ろしたのが今から一〇〇年以上前の明治二十九年、<br class="pc">
                        越後の国より開拓農民として入植以来、代々と続く農家からその技術を伝承しております。<br class="pc">現在は、自然豊かなこの地で、幻といわれる「ぼたんそば」にこだわり続けて<br class="pc">栽培された玄そばを当店で自家製粉し提供しております。</p>
                    <p class="font_hannari">　秀峰羊蹄山の青木の沢より流れ出る名水と、当日に必要なそばの実だけを<br class="pc">石臼により低速でじっくりと粗めに挽いたそば粉だけで打った十割そばは、<br class="pc">
                        最高の味わいを楽しめるお蕎麦として大変ご好評を頂いております。</p>

                </div>
                <!-- wrapper -->

            </section>
            <!-- about -->
            <section class="about_photo">

                <div class="polygon-top-right polygon-section naname blue"></div>
                <div class="polygon-bottom-left polygon-section  naname white"></div>


            </section>
            <!-- about_photo -->

            <section class="kodawari pb">
                <div class="wrapper pb_l">
                    <h3 class="headline1">そばのこだわり</h3>
                    <p class="pt_s font_hannari fsize pb_s">美味しいそば作りに欠かせないこだわりをご紹介します</p>
                    <p class="linkbtn1 pt_s pb_s mb"><a href="./kodawari.php">詳しくはこちら</a></p>

                    <div class="flexcol photo_left cf pt_s">
                        <div class="photo row1" style="background:url(./images/index_about1<?php mobile_img(); ?>.jpg) no-repeat center right;">
                        </div>
                        <!-- photo -->
                        <div class="text">
                            <h4 class="headline2 pb_s">雄大な羊蹄山の伏流水</h4>
                            <p>羊蹄山に蓄積した雪や雨水が長い時間を費やし<br class="pc">濾過された天然の伏流水を使用しております</p>
                        </div>
                        <!-- text -->
                    </div>
                    <!-- flexcol -->

                    <div class="flexcol photo_right pt cf content_up1">
                        <div class="photo row2" style="background:url(./images/index_about2<?php mobile_img(); ?>.jpg) no-repeat center left;">
                        </div>
                        <!-- photo -->
                        <div class="text">
                            <h4 class="headline2 pb_s">幻の品種 自家栽培「ぼたんそば」</h4>
                            <p>栽培が難しく幻の品種と評されるぼたんそばを<br class="pc">自家栽培し、店舗内工場にて製粉しております
                            </p>
                        </div>
                        <!-- text -->
                    </div>
                    <!-- flexcol -->

                    <div class="flexcol photo_left cf content_up2">
                        <div class="photo row3" style="background:url(./images/index_about3<?php mobile_img(); ?>.jpg) no-repeat center right;">
                        </div>
                        <!-- photo -->
                        <div class="text">
                            <h4 class="headline2 pb_s">毎日挽きたて・打ち立て・茹で立て</h4>
                            <p>打ち立て、茹で立てに加えて、店舗内で<br class="pc">石臼を使用し、挽き立てを提供しております。
                            </p>
                        </div>
                        <!-- text -->
                    </div>
                    <!-- flexcol -->
                </div>
                <!-- wrapper -->

            </section>
            <!-- kodawari -->

            <section class="soba bg_gray1">
                <div class="polygon-top-right polygon-section"></div>
                <div class="box-top"></div>
                <div class="outer">
                    <div class="wrapper">
                        <div class="title">
                            <h3 class="headline1 pb_s">二種類の手打そば</h3>
                            <p class="pb">当店では、そばの種類をお選びいただけます</p>
                        </div>
                        <ul class="grid_col2 ptn2 cf mb">
                            <li class="col box_ptn1 matchheight">
                                <h4 class="pb_s">十割そば</h4>
                                <p>つなぎ粉を使わず、幻のぼたんそばを自家栽培、石挽きしたそば粉だけで打ったそばは、芳醇な蕎麦の香りが楽しめます。</p>
                                <img class="sp" src="<?php echo $root_path; ?>images/youteisoba_sp.png" alt="">
                            </li>
                            <li class="col box_ptn1 matchheight">
                                <h4 class="pb_s">二八そば（田舎そば）</h4>
                                <p>二割の北海道産小麦のつなぎ粉と殻を含め挽きぐるみしたそば粉を使用。色黒で少し太めの強い味と香りが特徴の田舎そばです。</p>
                                <img class="sp" src="<?php echo $root_path; ?>images/wakimizusoba_sp.png" alt="">
                            </li>
                        </ul>
                        <h5>当店の十割そばは一般的な「十割そば＝田舎そば」<br class="pc">というイメージではなく、細切り、のど越しの良さが特徴です。<br>一方、二八そばは殻を含めた挽きぐるみのため、<br class="pc">田舎そばとして強い蕎麦の味を感じていただけます。</h5>
                        <p class="linkbtn1 pt_l"><a href="./menu.php">お品書きはこちら</a></p>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- outer -->
                <div class="box-bottom"></div>
            </section>
            <!-- soba -->

            <section class="net_store">
                <div class="polygon-top-left polygon-section"></div>

                <p><a href="http://webcheck3.x0.com/wakimizu/eccube/html/" target="_blank"><img src="./images/btn_netstore.jpg" alt="農家のそば　羊蹄山　オリジナル商品が、買える！ | 湧水の里 ネットストア"></a></p>
            </section>
            <!-- net_store -->

            <section class="store mt_l" id="access">
                <div class="title">
                    <h3 class="headline2 pb_s">アクセスマップ</h3>
                    <p>ご来店を心よりお待ちしております。</p>
                </div>
                <div class="map pt">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2923.54826435428!2d140.77466641547292!3d42.88237797915533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f120.1!3m3!1m2!1s0x5f0ab0b704cf84cd%3A0xefbb76927cabea4!2z6L6y5a6244Gu44Gd44Gw44KEIOe-iui5hOWxsQ!5e0!3m2!1sja!2sjp!4v1544769369786&ll=42.882453,140.776855&z=10" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </section>


            <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '4', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC',
      'category_name' => 'yoteizan' // 表示したいカテゴリーのスラッグを指定
    );
    $wp_query->query($param);?>
            <?php if($wp_query->have_posts()):?>


            <section class="news bg_blue1 pt pb">
                <div class="wrapper">
                    <h3>|&nbsp;NEWS&nbsp;|</h3>

                    <?php while($wp_query->have_posts()) :?>
                    <?php $wp_query->the_post(); ?>

                    <a href="<?php the_permalink() ?>">
                        <dl class="cf">
                            <dt><?php the_time('Y.m.d'); ?></dt>
                            <dd><?php echo $post->post_title; ?></dd>
                        </dl>
                    </a>

                    <?php endwhile; ?>

                </div>
                <!-- wrapper -->
            </section>
            <!-- news -->

            <?php endif; ?>
            <?php wp_reset_query(); ?>

            <section class="store_photo">
            </section>
            <!-- store_photo -->

        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
