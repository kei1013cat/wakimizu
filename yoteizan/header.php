<?php
  $url = $_SERVER['REQUEST_URI'];
  $tmp = explode("/", $url);
  $term_slug = end($tmp);
?>

<header id="top">
    <div class="pcmenu cf">
        <div class="header-top">
            <h1><a href="<?php echo $root_path; ?>">
                    <?php if(is_pc()): ?>
                    <img src="<?php echo $root_path; ?>images/header_kasou_logo.svg" alt="">
                    <?php endif; ?>
                </a></h1>

            <div class="header-right cf">
                <ol>
                    <?php if(is_pc()): ?>
                    <li class="lang pc" id="google_translate_element">
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'ja',
                                    includedLanguages: 'ja,en,fr,ko,ms,ru,th,zh-CN,zh-TW',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                }, 'google_translate_element');
                            }

                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        <!-- google翻訳後にレイアウト（高さ）がくずれるコンテンツをそろえる　-->
                        <div id="translationDetector" style="display:none;">English</div>
                        <script type="text/javascript">
                            var origValue = document.getElementById("translationDetector").innerHTML;
                            document.getElementById("translationDetector").addEventListener("DOMSubtreeModified", translationCallback, false);

                            function translationCallback() {
                                var currentValue = document.getElementById("translationDetector").innerHTML;
                                if (currentValue && currentValue.indexOf(origValue) < 0) {
                                    origValue = currentValue;
                                    setTimeout(function() {
                                        $('.sub-matchheight').matchHeight();
                                        $('.matchHeight').matchHeight();
                                    }, 1000);
                                }
                            }

                        </script>
                    </li>
                    <?php endif; ?>
                    <li><a href="https://facebook.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a></li>
                    <li><a href="https://www.instagram.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a></li>
                </ol>
            </div>
            <!-- header-right -->

            <nav>
                <ul class="cf">
                    <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                    <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>menu.php">お品書き</a></li>
                    <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>shop.php">店舗案内</a></li>
                    <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>index.php#access">アクセス</a></li>
                    <li><a class="hvr-underline-from-center" href="http://webcheck3.x0.com/wakimizu/eccube/html/" target="_blank">オンラインショップ</a></li>
                    <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
            </nav>

        </div>
        <!-- header-top -->
        <?php if(is_pc()): ?>
        <div class="header-bottom">
            <div class="polygon-inner">
                <div class="polygon-top-left polygon-section naname green top-minus"></div>
                <h2><a href="<?php echo $root_path; ?>">
                        <img src="<?php echo $root_path; ?>images/header_logo.svg" alt="農家のそばや 羊蹄山">
                    </a></h2>
            </div>
            <!-- header-bottom -->
            <div class="tel">
                <p x-ms-format-detection="none">
                    電　話：0136-21-2308<br>
                    定休日：水曜日
                </p>
            </div>
        </div>
        <!-- polygon-inner -->
        <?php endif; ?>

    </div>
    <!-- pcmenu -->

    <div class="spmenu drawermenu" role="banner" id="top">
        <div class="spmenu-outer">
            <span class="text">農家のそばや 羊蹄山</span>
            <h2 class="cf"><a href="<?php echo $root_path; ?>"><img src="<?php echo $root_path; ?>images/header_kasou_logo.svg" alt="農家のそばや 羊蹄山"></a></h2>
            <button type="button" class="drawer-toggle drawer-hamburger"> <span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span>
                <img class="menu-icon" src="<?php echo $root_path; ?>images/header_menu.svg">
            </button>
            <nav class="drawer-nav" role="navigation">
                <div class="inner">
                    <ul class="drawer-menu">
                        <li class="top cf">
                            <a class="facebook" href="https://facebook.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>
                            <a class="instagram" href="https://www.instagram.com/soba.yoteizan/" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a>
                            <?php if(is_mobile()): ?>

                            <div class="lang sp" id="google_translate_element">
                                <script type="text/javascript">
                                    function googleTranslateElementInit() {
                                        new google.translate.TranslateElement({
                                            pageLanguage: 'ja',
                                            includedLanguages: 'ja,en,fr,ko,ms,ru,th,zh-CN,zh-TW',
                                            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                        }, 'google_translate_element');
                                    }

                                </script>
                                <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                                <!-- google翻訳後にレイアウト（高さ）がくずれるコンテンツをそろえる　-->
                                <div id="translationDetector" style="display:none;">English</div>
                                <script type="text/javascript">
                                    var origValue = document.getElementById("translationDetector").innerHTML;
                                    document.getElementById("translationDetector").addEventListener("DOMSubtreeModified", translationCallback, false);

                                    function translationCallback() {
                                        var currentValue = document.getElementById("translationDetector").innerHTML;
                                        if (currentValue && currentValue.indexOf(origValue) < 0) {
                                            origValue = currentValue;
                                            setTimeout(function() {
                                                $('.matchHeight').matchHeight();
                                            }, 3000);
                                        }
                                    }

                                </script>
                            </div>
                            <?php endif; ?>
                        </li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>index.php">トップページ</a></li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>menu.php">お品書き</a></li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>shop.php">店舗案内</a></li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>index.php#access">アクセス</a></li>
                        <li><a class="drawer-menu-item" href="http://webcheck3.x0.com/wakimizu/eccube/html/" target="_blank">オンラインショップ</a></li>
                        <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                        <!--
                        <li class="text"><a class="drawer-menu-item" href="#">採用情報</a></li>
                        <li class="text"><a class="drawer-menu-item" href="#">会社情報</a></li>-->
                        <li class="text"><a class="drawer-menu-item" href="#">プライバシーポリシー</a></li>

                    </ul>
                    <div class="store_info">
                        <img src="<?php echo $root_path; ?>images/spmenu_store.jpg">
                        <h3>農家のそば 羊蹄山</h3>
                        <p class="ad"><a href="https://www.google.co.jp/maps/place/%E8%BE%B2%E5%AE%B6%E3%81%AE%E3%81%9D%E3%81%B0%E3%82%84+%E7%BE%8A%E8%B9%84%E5%B1%B1/@42.882378,140.7746664,17z/data=!3m1!4b1!4m5!3m4!1s0x5f0ab0b704cf84cd:0xefbb76927cabea4!8m2!3d42.882378!4d140.7768551?shorturl=1" target="_blank">北海道虻田郡倶知安町字富士見463-1</a></p>
                        <p x-ms-format-detection="none"><a href="tel:0136-21-2308">0136-21-2308</a></p>
                    </div>
                    <!-- store_info -->

                </div>
                <!--inner -->
            </nav>
        </div>
        <!-- spmenu-outer -->
    </div>
    <!-- spmenu -->
</header>
<main role="main">
