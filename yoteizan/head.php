<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MZGXWVJ');</script>
<!-- End Google Tag Manager -->

<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keyword; ?>" />
<meta name="format-detection" content="telephome=no">

<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?php echo $root_path; ?>css/sp.css" media="screen and (max-width: 767px)">
<link rel="stylesheet" href="<?php echo $root_path; ?>css/pc.css" media="screen and (min-width: 768px)">


<script src="<?php echo $root_path; ?>js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/iscroll.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo $root_path; ?>js/sp_switch.js"></script> -->
<link rel="stylesheet" href="<?php echo $root_path; ?>js/Hover/css/hover.css">

<link rel="stylesheet" href="<?php echo $root_path; ?>js/drawer/dist/css/drawer.min.css">
<script type="text/javascript" src="<?php echo $root_path; ?>js/drawer/dist/js/drawer.min.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/iscroll.min.js"></script>
<!--<script type="text/javascript" src="<?php echo $root_path; ?>js/smoothScroll.js"></script>-->

<!-- favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $root_path; ?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $root_path; ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $root_path; ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#4f5659">
<meta name="msapplication-TileColor" content="#d8dd7a">
<meta name="theme-color" content="#d8dd7a">

<?php if(is_pc()): ?>
<script type="text/javascript" src="<?php echo $root_path; ?>js/match-height/dist/jquery.matchHeight-min.js"></script>
<script>
jQuery(function($) {
    $('.matchheight').matchHeight();
});
</script>

<?php endif; ?>
<script>
$(document).ready(function() {
    $('.drawer').drawer();
});
</script>


<script type="text/javascript">
<?php if(is_mobile()): ?>
var headerHeight = 60;
<?php else: ?>
var headerHeight = 100;
<?php endif; ?>

var urlHash = location.hash;
if(urlHash) {
    $('body,html').stop().scrollTop(0);
    setTimeout(function(){
        var target = $(urlHash);
        var position = target.offset().top - headerHeight;
        $('body,html').stop().animate({scrollTop:position}, 500);
    }, 1000);
}
$('.link').click(function() {
    var href= $(this).attr("href");
    var target = $(href);
    var position = target.offset().top - headerHeight;
    $('body,html').stop().animate({scrollTop:position}, 500);   
});
</script>


<!-- トップのスライダー画像の設定 Start -->
<link rel="stylesheet" href="<?php echo $root_path; ?>/js/vegas/vegas.css">
<script src="<?php echo $root_path; ?>/js/vegas/vegas.js"></script>
<script>
$(function(){
    $('#mainvisual').vegas({
        slides: [
        //********　トップスライダーの画像を追加する場合はここ　************************************************
        // mobile_img()はスマホの場合に「_sp」を追加する処理です。スマホ版の画像は「_sp」の画像を追加してください
        { src: './images/mainimg1<?php echo mobile_img(); ?>.jpg'},
        { src: './images/mainimg2<?php echo mobile_img(); ?>.jpg'},
        { src: './images/mainimg3<?php echo mobile_img(); ?>.jpg'},
        { src: './images/mainimg4<?php echo mobile_img(); ?>.jpg?v=20181218'},
        //**************************************************************************************************
        ],
        // 初期設定時のコールバック関数（カスタマイズ追加）
        init: function(){
            // インジゲーターを追加する
            var slides = $('#mainvisual').vegas('options', 'slides');        
            var data = "";
            for (i=0; i <= slides.length - 1; i++) {
                data = data + '<div class="vegas-pager-item"><a href="javascript:void(0)" data-slide-index="' + i + '" class="vegas-pager-link">' + i + '</a></div>';
            }        
            $("#mainvisual .vegas-pager").append(data);

        },
        // 画像切り替わり時（オートプレイ）のコールバック関数（カスタマイズ追加）
        walk: function(index, slideSettings){
            // オートプレイで切り替わった際のインジゲーターのアクティブ状態を変更する
            $('a.vegas-pager-link').removeClass('active');
            $("[data-slide-index='" + index + "']").addClass('active');
        },
        // vegas.jsのオプション設定 ////////////
        overlay: false,
        transition: 'fade2',
        transitionDuration: 1500,
        delay: 6000,
        animationDuration: 18000,
        timer:false
        //////////////////////////////////////////
    });
    // インジゲーターのクリックイベント処理（カスタマイズ追加）
    $(document).on('click', '.vegas-pager-link' ,function() {
        var $elmt = $('#mainvisual');
        $elmt.vegas('jump',$(this).data('slide-index'));
        $('a.vegas-pager-link').removeClass('active');
        $(this).addClass('active');
    });
    // ブラウザの幅に合わせて高さを設定
	w = $(window).width();
	if (w >= 768) {
	    //画面高さ取得
	    h = $(window).height();
	    $("#mainvisual").css("height", h - 60 + "px");
	}

});
</script>
<!-- トップのスライダー画像の設定 End -->

<script>
jQuery(function($){
var ua = navigator.userAgent;
var $scrtgt = $(window); // スクロール対象：<html>
if ( ua.indexOf( 'MSIE' ) > 0 || ua.indexOf( 'Trident' ) > 0 ) {
    jQuery('#page_shop section.bottom .photo').css('background-attachment','scroll');
}
if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
    jQuery('#page_shop section.bottom .photo').css('background-attachment','scroll');
}

});
    

</script>



<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="" />
<meta property="og:image" content="" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:site_name" content="" />