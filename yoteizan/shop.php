<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "店舗案内｜【公式】農家のそばや 羊蹄山";
$description = "水面に映る羊蹄山を一望する絶景のロケーション。ニセコの大自然に抱かれた店内で、ゆっくりと本物のそばをご堪能いただけます。";
$keyword = "北海道,倶知安,蕎麦,ぼたんそば,コロッケ,ようてい,農家のそばや,羊蹄山,手打ちそば,十割そば,二八そば,冷そば,温そば,ビール,酒,ネットストア";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_shop" class="subpage drawer drawer--right drawer-close">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">
            <?php include_once "pan.php"; ?>

            <section class="info">
                <div class="wrapper">
                    <h3 class="headline4 mt_l mb">基本情報</h3>
                    <div class="outer cf">
                        <div class="left">
                            <img src="./images/shop_info_photo1.jpg">
                        </div>
                        <!-- left -->
                        <div class="right">
                            <h3>農家のそばや　羊蹄山（ようていざん）</h3>
                            <address class="pt_s">
                                <dl class="cf">
                                    <dt>住　　所：</dt>
                                    <dd>〒044-0075&nbsp;<br class="sp">北海道虻田郡倶知安町字富士見463-1</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>電話番号：</dt>
                                    <dd x-ms-format-detection="none"><a href="tel:0136-21-2308">0136-21-2308</a>　<span class="small">※予約不可</span></dd>
                                </dl>
                                <dl class="cf">
                                    <dt>営業時間：</dt>
                                    <dd>【夏期（4～10月）】11：00～15：30<br>【冬期（11～3月）】11：00～14：30<br>※売り切れ次第終了する場合がございます。</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>定&nbsp;&nbsp;休&nbsp;&nbsp;日：</dt>
                                    <dd>【夏期（4～10月）】水曜日<br>【冬期（11～3月）】水曜日・木曜日</dd>
                                </dl>
                            </address>

                        </div>
                        <!-- right -->
                    </div>
                    <!-- outer -->
                    <div class="map pt">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2923.54826435428!2d140.77466641547292!3d42.88237797915533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f120.1!3m3!1m2!1s0x5f0ab0b704cf84cd%3A0xefbb76927cabea4!2z6L6y5a6244Gu44Gd44Gw44KEIOe-iui5hOWxsQ!5e0!3m2!1sja!2sjp!4v1544769369786&ll=42.882453,140.776855&z=10" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <!-- map -->
                </div>
                <!-- wrapper -->
            </section>

            <section class="hitokoto pb_l">
                <div class="wrapper">
                    <h3 class="headline4 mb_s">店舗からひとこと</h3>
                    <p class="pb_l">当店に使用している木材は100年以上前の建物の解体する際に出たものです。<br>
                        落ち着いた空間、羊蹄山を眺めながら水辺のあるくつろぎの時間を楽しんでいただけたら幸いです。<br>
                        挽き立て、打ちたて、ゆでたての香り高い本物のそばを是非味わってください。<br>
                        店員一同心よりお待ちしております。</p>

                    <img class="pt" src="./images/shop_hitokoto_photo1.jpg">
                    <div class="grid_col2 ptn2 cf">
                        <div class="col left">
                            <img src="./images/shop_hitokoto_photo2.jpg">
                        </div>
                        <!-- col -->
                        <div class="col right">
                            <img src="./images/shop_hitokoto_photo3.jpg">
                        </div>
                        <!-- col -->
                    </div>
                    <!-- grid_col2 -->
                    <div class="grid_col2 ptn2 cf pb">
                        <div class="col left">
                            <img src="./images/shop_hitokoto_photo4.jpg">
                        </div>
                        <!-- col -->
                        <div class="col right">
                            <img src="./images/shop_hitokoto_photo5.jpg">
                        </div>
                        <!-- col -->
                    </div>
                    <!-- grid_col2 -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- hitokoto -->

            <section class="bottom">
                <div class="photo mt_l"></div>
            </section>
            <!-- bottom -->

            <section class="net_store mt_l mb_l">
                <a href="http://www.youteizan.com/cgi-bin/eshop/e_shop.cgi?bunrui=all&keyword=&superkey=1&FF=0" target="_blank"><img class="pb_l" src="./images/btn_netstore.jpg" alt="農家のそば　羊蹄山　オリジナル商品が、買える！ | 湧水の里 ネットストア"></a>
            </section>
            <!-- net_store -->

            <section id="bottom_link">
                <div class="wrapper">
                    <ul class="grid_col3 cf pb">

                        <li class="col">
                            <a href="<?php echo $root_path; ?>kodawari.php">
                                <div class="photo photo1">
                                    <img src="./images/bottom_link_photo4.jpg">
                                    <div class="text">そばのこだわり</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>menu.php">
                                <div class="photo photo2">
                                    <img src="./images/bottom_link_photo1.jpg">
                                    <div class="text">お品書き</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>index.php#access">
                                <div class="photo photo2">
                                    <img src="./images/bottom_link_photo2.jpg">
                                    <div class="text">アクセス</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
            <!-- bottom_link -->

        </div>
        <!-- contents -->



        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
