<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "お品書き｜【公式】農家のそばや 羊蹄山";
$description = "幻の牡丹そばを使用した、十割そばと二八そばをご用意しています。自家栽培・自家製粉の手打ちならではの、ぼたんそば特有の味と香りをお楽しみください。";
$keyword = "北海道,倶知安,蕎麦,ぼたんそば,コロッケ,ようてい,農家のそばや,羊蹄山,手打ちそば,十割そば,二八そば,冷そば,温そば,ビール,酒,ネットストア";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_menu" class="subpage drawer drawer--right drawer-close">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">
            <?php include_once "pan.php"; ?>

            <section class="menu pt_l">
                <div class="wrapper">
                    <div class="grid_col2 ptn2 cf mb">
                        <h3 class="left headline4 col mt">お品書き</h3>
                        <p class="right col"><a href="./images/english_menu.pdf" target="_blank"><img src="./images/btn_english_menu.jpg" alt="English menu is here"></a></p>
                    </div>
                    <!-- grid_col2 -->
                </div>
                <!-- wrapper -->

                <div class="soba bg_gray1 pt pb">
                    <div class="outer pt_l pb_l">
                        <div class="wrapper">
                            <div class="title">
                                <h3 class="headline1 pb_s">二種類の手打そば</h3>
                                <p class="pb">当店では、そばの種類をお選びいただけます</p>
                            </div>
                            <ul class="grid_col2 ptn2 cf mb ">
                                <li class="col box_ptn1 matchheight">
                                    <h4 class="pb_s">十割そば</h4>
                                    <p>つなぎ粉を使わず、幻のぼたんそばを自家栽培、石挽きしたそば粉だけで打ったそばは、芳醇な蕎麦の香りが楽しめます。</p>
                                    <img class="sp" src="<?php echo $root_path; ?>images/youteisoba_sp.png" alt="">
                                </li>
                                <li class="col box_ptn1 matchheight">
                                    <h4 class="pb_s">二八そば（田舎そば）</h4>
                                    <p>二割の北海道産小麦のつなぎ粉と殻を含め挽きぐるみしたそば粉を使用。色黒で少し太めの強い味と香りが特徴の田舎そばです。</p>
                                    <img class="sp" src="<?php echo $root_path; ?>images/wakimizusoba_sp.png" alt="">
                                </li>
                            </ul>
                            <h5>当店の十割そばは一般的な「十割そば＝田舎そば」<br class="pc">というイメージではなく、細切り、のど越しの良さが特徴です。<br>一方、二八そばは殻を含めた挽きぐるみのため、<br class="pc">田舎そばとして強い蕎麦の味を感じていただけます。</h5>
                        </div>
                        <!-- wrapper -->
                    </div>
                    <!-- outer -->
                </div>
                <!-- outer -->
            </section>
            <!--- menu -->

            <section class="hiyashi_soba">
                <div class="wrapper">
                    <h3 class="headline5 underline mt_l">冷そば</h3>
                    <div class="menu_ptn1 cf">
                        <div class="left pt_l">
                            <img src="./images/menu_photo1.jpg">
                            <p class="capiton mb_s">せいろそば</p>
                            <img src="./images/menu_photo2.jpg">
                            <p class="capiton mb_s">エビ天 おろしそば</p>
                            <img src="./images/menu_photo3.jpg">
                            <p class="capiton mb_s">鴨せいろそば</p>
                        </div>
                        <!-- left -->
                        <div class="right mt_s">

                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th></th>
                                    <th class="you">【十割そば】</th>
                                    <th class="waki">【二八そば】</th>
                                </tr>
                                <tr>
                                    <td class="item">せいろそば</td>
                                    <td class="you">800円</td>
                                    <td class="waki">700円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">そばの味、香りが一番良くわかります</td>
                                </tr>
                                <tr>
                                    <td class="item">二段重ね せいろそば</td>
                                    <td class="you">1,250円</td>
                                    <td class="waki">1,100円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">二人前　※大盛りにはできません。</td>
                                </tr>
                                <tr>
                                    <td class="item">ごまだれ せいろそば</td>
                                    <td class="you">900円</td>
                                    <td class="waki">800円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">国内産釜いりごま使用</td>
                                </tr>
                                <tr>
                                    <td class="item">おろしそば</td>
                                    <td class="you">1,000円</td>
                                    <td class="waki">900円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">季節により自家栽培の辛味大根を使用</td>
                                </tr>
                                <tr>
                                    <td class="item">つけとろそば</td>
                                    <td class="you">1,050円</td>
                                    <td class="waki">950円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">のど越しなめらか、栄養たっぷりです</td>
                                </tr>
                                <tr>
                                    <td class="item">山菜きのこ おろしそば</td>
                                    <td class="you">1,200円</td>
                                    <td class="waki">1,100円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">地場産のきのこを使用しております</td>
                                </tr>
                                <tr>
                                    <td class="item">やまかけ にしんそば</td>
                                    <td class="you">1,250円</td>
                                    <td class="waki">1,150円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">余市丸イ伊藤商店のにしんを使用</td>
                                </tr>
                                <tr>
                                    <td class="item">エビ天 おろしそば</td>
                                    <td class="you">1,250円</td>
                                    <td class="waki">1,150円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">海老天２本のぶっかけスタイル</td>
                                </tr>
                                <tr>
                                    <td class="item">鴨せいろそば</td>
                                    <td class="you">1,450円</td>
                                    <td class="waki">1,350円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">北海道滝川産のほっかい合鴨を使用</td>
                                </tr>
                                <tr>
                                    <td class="item">エビ天 せいろそば</td>
                                    <td class="you">1,500円</td>
                                    <td class="waki">1,400円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">海老天に加えて、野菜天、きのこ天</td>
                                </tr>
                            </table>
                            <p class="oomori">（大盛り200円増し）</p>

                        </div>
                        <!-- right -->

                    </div>
                    <!-- menu_ptn1 -->


                    <h3 class="headline5 underline mt_l">温そば</h3>

                    <div class="menu_ptn1 cf">
                        <div class="left pt_l">
                            <img src="./images/menu_photo4.jpg">
                            <p class="capiton mb_s">きつねそば</p>
                            <img src="./images/menu_photo5.jpg">
                            <p class="capiton mb_s">にしんそば</p>
                        </div>
                        <!-- left -->
                        <div class="right mt_s">

                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <th></th>
                                    <th class="you">【十割そば】</th>
                                    <th class="waki">【二八そば】</th>
                                </tr>
                                <tr>
                                    <td class="item">かけそば</td>
                                    <td class="you">800円</td>
                                    <td class="waki">700円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">そばの美味しさが良くわかります</td>
                                </tr>
                                <tr>
                                    <td class="item">月見そば</td>
                                    <td class="you">880円</td>
                                    <td class="waki">780円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">石川さんの玉子を使用しております</td>
                                </tr>
                                <tr>
                                    <td class="item">きつねそば</td>
                                    <td class="you">950円</td>
                                    <td class="waki">850円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">真狩湧水の里豆腐工房の油揚げ使用</td>
                                </tr>
                                <tr>
                                    <td class="item">やまかけそば</td>
                                    <td class="you">1,000円</td>
                                    <td class="waki">900円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">やまかけたっぷり、栄養満点です</td>
                                </tr>
                                <tr>
                                    <td class="item">山菜きのこそば</td>
                                    <td class="you">1,100円</td>
                                    <td class="waki">1,000円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">山菜三種と地場産きのこ三種使用</td>
                                </tr>
                                <tr>
                                    <td class="item">にしんそば</td>
                                    <td class="you">1,150円</td>
                                    <td class="waki">1,050円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">余市丸イ伊藤商店のにしんを使用</td>
                                </tr>
                                <tr>
                                    <td class="item">エビ天そば</td>
                                    <td class="you">1,200円</td>
                                    <td class="waki">1,100円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">海老の天ぷらは別盛にて提供</td>
                                </tr>
                                <tr>
                                    <td class="item">鴨南そば</td>
                                    <td class="you">1,450円</td>
                                    <td class="waki">1,350円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="3">北海道滝川産のほっかい合鴨を使用</td>
                                </tr>
                            </table>
                            <p class="oomori">（大盛り200円増し）</p>
                        </div>
                        <!-- right -->

                    </div>
                    <!-- menu_ptn1 -->

                    <h3 class="headline5 underline mt_l mb_l">一品料理</h3>

                    <div class="grid_col2 ptn2 menu_ptn2 cf">
                        <div class="left col">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="item">エビ天一尾</td>
                                    <td class="en">200円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">にしんの姿煮</td>
                                    <td class="en">450円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">野菜ときのこの天ぷら</td>
                                    <td class="en">600円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                        <!-- left -->
                        <div class="right col">

                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="item">いなりずし　二個</td>
                                    <td class="en">350円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">そばがき揚げ</td>
                                    <td class="en">480円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">エビ天ぷら</td>
                                    <td class="en">730円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                            </table>

                        </div>
                        <!-- right -->
                    </div>
                    <!-- menu_ptn2 -->
                    <ul class="grid_col2 ptn2 cf pc pt_s pb_l">
                        <li class="col"><img src="./images/menu_photo6.jpg">
                            <p>野菜ときのこの天ぷら</p>
                        </li>
                        <li class="col"><img src="./images/menu_photo7.jpg">
                            <p>そばがき揚げ</p>
                        </li>
                    </ul>


                    <h3 class="headline5 underline mt_l mb_l">お飲物</h3>

                    <div class="grid_col2 ptn2 menu_ptn2 cf mb_l">
                        <div class="left col tab1">
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="item">お酒　（冷・燗）</td>
                                    <td class="en">550円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">ノンアルコールビール</td>
                                    <td class="en">380円</td>
                                </tr>
                            </table>
                        </div>
                        <!-- left -->
                        <div class="right col tab2">

                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="item">瓶ビール</td>
                                    <td class="en">540円</td>
                                </tr>
                                <tr>
                                    <td class="text" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td class="item">ジュース</td>
                                    <td class="en">250円</td>
                                </tr>
                            </table>
                        </div>
                        <!-- right -->
                    </div>
                    <!-- menu_ptn2 -->

                    <p class="linkbtn pt mb"><a href="./images/english_menu.pdf" target="_blank"><img src="./images/btn_english_menu.jpg" alt="English menu is here"></a></p>

                </div>
                <!-- wrapper -->
            </section>
            <!-- hiyashi_soba -->


            <section class="kome pt_l pb_l bg_gray1">
                <div class="wrapper">
                    <ul>
                        <li>※メニュー価格は消費税込の総額表示価格です。</li>
                        <li>※温かいそば等をお召し上がりの際にはやけどにご注意ください。</li>
                        <li>※未成年者及び車両等を運転してお帰りのお客様には、アルコール類のご提供はできませんのでご了承ください。</li>
                        <li>※当店の揚げ物料理は植物油を使用しています。</li>
                    </ul>
                </div>
                <!-- wrapper -->
            </section>


            <section class="net_store mt_l mb_l">
                <a href="http://www.youteizan.com/cgi-bin/eshop/e_shop.cgi?bunrui=all&keyword=&superkey=1&FF=0" target="_blank"><img class="pb_l" src="./images/btn_netstore.jpg" alt="農家のそば　羊蹄山　オリジナル商品が、買える！ | 湧水の里 ネットストア"></a>
            </section>
            <!-- net_store -->

            <section id="bottom_link">
                <div class="wrapper">
                    <ul class="grid_col3 cf pb">

                        <li class="col">
                            <a href="<?php echo $root_path; ?>kodawari.php">
                                <div class="photo photo1">
                                    <img src="./images/bottom_link_photo4.jpg">
                                    <div class="text">そばのこだわり</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>index.php#access">
                                <div class="photo photo2">
                                    <img src="./images/bottom_link_photo2.jpg">
                                    <div class="text">アクセス</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>shop.php">
                                <div class="photo photo3">
                                    <img src="./images/bottom_link_photo3.jpg">
                                    <div class="text">店舗案内</div>
                                    <div class="bg"></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
            <!-- bottom_link -->

        </div>
        <!-- contents -->


        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
