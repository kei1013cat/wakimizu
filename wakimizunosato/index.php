<?php require('../cms/wp-load.php'); ?>
<?php
$root_path = "./";
$title = "【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";
$ranking_postid = 71;

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_index" class="drawer drawer--right drawer-close">

    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <div id="contents">

            <?php //PC版 ?>
            <section class="mainimg">
                <video class="masthead-video" poster="./images/mainimg.jpg" autoplay loop muted>
                    <source src="./images/movie.mp4" type="video/mp4">
                </video>
                <div class="mainimg_text cf enter-left">
                    <div class="text-left"></div>
                    <div class="text-right">
                        つくる夢、豆にこだわり、水をもとめてたどりついた名水。<br>
                        羊蹄山麓から湧き出る水で、つくり上げるお豆腐。
                    </div>
                </div>
                <img class="cont1 enter-bottom2" src="./images/mainimg_cont1.png">
                <img class="cont2 enter-bottom3" src="./images/mainimg_cont2.png">
                <img class="cont3 enter-bottom4" src="./images/mainimg_cont3.png">
            </section>

            <?php //スマホ版 ?>
            <section class="mainimg-sp">
                <div class="sp">
                    <div class="mainimg_text cf enter-left">
                        つくる夢、豆にこだわり、<br class="sp">水をもとめてたどりついた名水。<br>
                        羊蹄山麓から湧き出る水で、<br class="sp">つくり上げるお豆腐。
                    </div>
                    <img class="cont1 enter-bottom2" src="./images/mainimg_cont_sp.png">
                </div>
            </section>
            <!-- mainimg-sp -->

            <section class="about pt_l pb">
                <div class="wrapper">
                    <div class="flexcol photo_left pt pb_l cf">
                        <div class="photo" style="background:url(./images/index_top_photo1.jpg) no-repeat;">
                        </div>
                        <!-- photo -->
                        <div class="text googtrans-box">
                            <h4>匠の技、冴える</h4>
                            <p>
                                先代と職人たちが長い年月をかけ<br class="pc">
                                技術を磨き、幾度となく失敗を<br class="pc">
                                繰り返し、こんにちの豆腐があります。<br class="pc">
                                今より美味しい豆腐をもとめて、<br class="pc">
                                日々試行錯誤しております。<br>
                                <a class="linkbtn" href="./kodawari.php">こだわりを詳しく見る</a>
                            </p>
                        </div>
                        <!-- text -->
                    </div>
                    <!-- grid-col2 -->

                    <div class="flexcol photo_right pt_l cf">
                        <div class="photo" style="background:url(./images/index_top_photo2.jpg) no-repeat;">
                        </div>
                        <!-- photo -->
                        <div class="text googtrans-box">
                            <h4>豆腐品評会 受賞</h4>
                            <p>
                                名誉ある豆腐の品評会。<br class="pc">
                                より良い豆腐を目指したい、<br class="pc">
                                そんな想いから一つの指標として<br class="pc">
                                積極的に品評会に出品しています。<br class="pc"><br>

                                2017年　農研機構賞受賞<br>
                                2018年　北海道豆腐品評会最優秀賞<br>
                                2019年　豆腐マイスター賞受賞
                            </p>
                        </div>
                        <!-- text -->
                    </div>
                    <!-- grid-col2 -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- about -->


            <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '3', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC',
      'category_name' => 'wakimizunosato' // 表示したいカテゴリーのスラッグを指定
    );
    $wp_query->query($param);?>
            <?php if($wp_query->have_posts()): ?>

            <section class="topics pt pb_l bg_beige">
                <div class="wrapper pb">
                    <h3 class="headline1"><span>トピックス</span></h3>
                    <div class="pt_l">
                        <ul class="cf grid_col">

                            <?php while($wp_query->have_posts()) :?>
                            <?php $wp_query->the_post(); ?><li class="col matchheight">
                                <div class="box bg_white">
                                    <?php if(has_post_thumbnail()): ?>
                                    <?php the_post_thumbnail('topics_wakimizusato'); ?>
                                    <?php else: ?>
                                    <img src="./images/noimage.png" alt="no image">
                                    <?php endif; ?>
                                    <p class="date fsize_s"><?php the_time('Y.m.d'); ?></p>
                                    <h3><?php echo mb_substr($post->post_title,0, 16)."..."; ?></h3>
                                    <div class="text">
                                        <?php echo mb_substr(get_the_excerpt(), 0, 59)."..."; ?>
                                    </div>
                                    <p class="linkbtn1"><a href="<?php the_permalink() ?>">詳しくはこちら</a></p>
                                </div>
                                <!-- box -->
                            </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
                <!-- wrapper -->
            </section>
            <!-- topics -->

            <?php endif; ?>
            <?php wp_reset_query(); ?>


            <?php if( have_rows('人気ランキング',$ranking_postid)): ?>
            <section class="ranking pt_l">
                <h3 class="headline1"><span>人気ランキング</span></h3>
                <div class="outer mt_l pt_l pb">
                    <div class="wrapper">
                        <ul class="grid_col3 cf">
                            <?php $rank_num = 1; ?>
                            <?php while( have_rows('人気ランキング',$ranking_postid) ): the_row(); ?>
                            <li class="col rank<?php echo $rank_num; ?>">
                                <?php $ranking_img = get_sub_field('画像'); ?>
                                <img class="img_center" src="<?php echo $ranking_img['sizes']['ranking_img'];?>" alt="<?php echo the_title(); ?>">
                                <p class="text pt_s"><?php echo get_sub_field('タイトル'); ?></p>
                            </li>
                            <?php $rank_num++; ?>
                            <?php
                              if($rank_num==4) {
                                break;
                              }
                            ?>
                            <?php endwhile;
                             ?>
                        </ul>
                        <p class="linkbtn1 mt_l mb">
                            <a href="./products.php">商品一覧を詳しく見る</a>
                        </p>
                    </div>
                    <!-- wrapper -->
                </div>
                <!-- outer -->
            </section>
            <!-- ranking -->
            <?php endif; ?>

            <section class="net_store pt pb_l">
                <div class="wrapper">
                    <p><a href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
                </div>
                <!-- wrapper -->
            </section>
            <!-- net_store -->

            <section class="access pt_l bg_beige">
                <h3 class="headline1 pb_l"><span>アクセス</span></h3>
                <div class="wrapper">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2928.2467616477734!2d140.77672231584347!3d42.783132979160854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f130.1!3m3!1m2!1s0x5f0aad65b002945f%3A0x31b05da12002e856!2z5rmn5rC044Gu6YeMIOecn-eLqeixhuiFkOW3peaIvw!5e0!3m2!1sja!2sjp!4v1547116216475" frameborder="0" style="border:0" allowfullscreen></iframe>
                    <div class="access_bottom mt pb_l">
                        <div class="outer cf">
                            <div class="col1">
                                自家用車で
                            </div>
                            <!-- col1 -->
                            <div class="col2">
                                ●札幌から（中山峠経由）<br>
                                車／国道230号線、道道66号線経由で2時間<br><br>

                                ●小樽から<br>
                                車／国道5号線、道道66号線経由で1時間40分<br><br>

                                ●新千歳空港から（支笏湖経由）<br>
                                車／道道16号線、国道276号線、国道230号線、道道66号線経由で2時間
                            </div>
                            <!-- col2 -->
                            <div class="col3">
                                ●比羅夫（ヒラフ）エリアから<br>
                                車／道道631号線、国道5号線、道道66号線経由で20分<br><br>

                                ●JRニセコ駅から<br>
                                車／道道66号線で12分
                            </div>
                            <!-- col3 -->
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- access_bottom -->
                </div>
                <!-- wrapper -->
            </section>
            <!-- access -->

            <section class="shishoku pt pb_l">
                <h3 class="headline1 pb_l"><span>試食情報</span></h3>
                <div class="wrapper">
                    <img src="./images/index_shishoku.jpg">
                    <p class="mt">
                        私たちは、一つ一つの豆腐にこだわりとアイデアを注ぎ込んで商品開発を行っております。<br>
                        たくさんの種類の中から、お気に入りを選んでもらいたい、そんな想いから基本的に全商品ご試食いただけるように試食コーナーをご用意しています。<br><br>
                        ※繁忙期、時間帯や曜日により、試食を行っていないこともございますがご了承ください<br>
                        ※試食に関するお問合せは受けかねますが、ご了承ください。</p>
                </div>
                <!-- wrapper -->
            </section>
            <!-- shishoku -->

            <section class="wakimizu bg_beige pt pb_l">
                <h3 class="headline1 pb_l"><span>湧水について</span></h3>
                <div class="wrapper">
                    <img src="./images/index_wakimizu.jpg">
                    <p class="mt">
                        店舗前には、数十年の歳月をかけて生まれる、ミネラルたっぷりの真狩の水が岩肌から勢いよく湧き出しています。羊蹄山に降った雨や雪溶け水が生まれ変わった天然の水は、夏場でも冷たく、凛とした飲み口です。<br><br>
                        ※同敷地内の飲用可能な湧水は当店の管理ではございません。</p>
                </div>
                <!-- wrapper -->
            </section>
            <!-- wakimizu -->


            <section class="other_store pb_l pt">
                <h3 class="headline1 pb_l"><span>こんなお店も</span></h3>
                <div class="wrapper">
                    <ul class="outer cf">
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../haru/" target="_blank">
                                <img src="./images/index_haru.jpg">
                                <h4>石窯パンマルシェ HARU</h4>
                                <p>地元の食材を使ったパンを揃えています。羊蹄山を一望するテラス＆カフェも併設。</p>
                            </a>
                        </li>
                        <li class="col">
                            <a href="<?php echo $root_path; ?>../yoteizan/" target="_blank">
                                <img src="./images/index_yoteizan.jpg">
                                <h4>農家のそばや 羊蹄山</h4>
                                <p>羊蹄山の湧き水で仕上げる、自家栽培・自家製粉にこだわった手打ちそばをご提供</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- wrapper -->
            </section>
            <!-- other_store -->


        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
