<!-- Google Tag Manager -->
<!--
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MZGXWVJ');</script>
-->
<!-- End Google Tag Manager -->

<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keyword; ?>" />
<meta name="format-detection" content="telephome=no">

<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?php echo $root_path; ?>css/sp.css" media="screen and (max-width: 767px)">
<link rel="stylesheet" href="<?php echo $root_path; ?>css/pc.css" media="screen and (min-width: 768px)">


<!--<script src="<?php echo $root_path; ?>js/jquery-3.3.1.min.js"></script>-->
<script src="<?php echo $root_path; ?>js/jquery-1.11.3.min.js"></script>

<script type="text/javascript" src="<?php echo $root_path; ?>js/smoothScroll.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/sp_switch.js"></script>

<link rel="stylesheet" href="<?php echo $root_path; ?>js/drawer/dist/css/drawer.min.css">
<script type="text/javascript" src="<?php echo $root_path; ?>js/drawer/dist/js/drawer.min.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/iscroll.min.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/header_parallax.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/match-height/dist/jquery.matchHeight-min.js"></script>
<link rel="stylesheet" href="<?php echo $root_path; ?>js/Hover/css/hover.css">

<!-- scrollreveal -->
<script type="text/javascript" src="<?php echo $root_path; ?>js/scrollreveal/scrollreveal.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/scrollreveal/scrollreveal.thema.js"></script>

<!-- bxslider -->
<link rel="stylesheet" href="<?php echo $root_path; ?>js/bxslider/dist/jquery.bxslider.css">
<script type="text/javascript" src="<?php echo $root_path; ?>js/bxslider/dist/jquery.bxslider.js"></script>
<script type="text/javascript" src="<?php echo $root_path; ?>js/bxslider_setting.js"></script>

<!-- favicon -->
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $root_path; ?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $root_path; ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $root_path; ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo $root_path; ?>/images/favicon/site.webmanifest">
<link rel="mask-icon" href="<?php echo $root_path; ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<script>
jQuery(function($) {
    $('.matchheight').matchHeight();
    $('.sub-matchheight').matchHeight();

});
</script>
<script>
$(document).ready(function() {
    $('.drawer').drawer();
});
</script>

<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:url" content="" />
<meta property="og:image" content="" />
<meta property="og:description" content="<?php echo $description; ?>" />
<meta property="og:locale" content="ja_JP" />
<meta property="og:site_name" content="" />