jQuery(function($){

  $(".main-slider").each(function(index, el) {
    $(this).attr('id', 'bx-slider' + index);
  });

  $(".bx-slider-thum").each(function(index, el) {
    $(this).attr('id', 'bx-slider-thum' + index);
  });

  $(".main-slider").each(function(index, el) {
    $(this).attr('id', 'bx-slider' + index);

    var slider01 = $('#bx-slider' + index).bxSlider({
        pagerCustom: '#bx-slider-thum' + index,
        mode: 'fade',
        captions: true,
        controls: false,
        onSlideBefore: function($slideElement, oldIndex, newIndex) {
            slider02.goToSlide(newIndex);
        }
    });

    var slider02 = $('#bx-slider-thum' + index).bxSlider({
        pager: false,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        slideWidth: 140,
        slideMargin: 15,
        onSlideBefore : function($slideElement, oldIndex, newIndex){
            slider02.find('[data-slide-index]').removeClass("act");
            slider02.find('[data-slide-index="' + (newIndex) + '"]').addClass("act");
          }
    });
  });
});
