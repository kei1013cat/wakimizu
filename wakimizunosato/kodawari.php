<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "こだわり｜【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_kodawari" class="subpage drawer drawer--right drawer-close">

<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
  <?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">
      
      
      <div class="wrapper">
        <section class="kodawari1 pb_l mb_l">
            <h3 class="headline2 mb_l mt_l">契約農場で栽培される<br class="sp">ここだけの豆</h3>
            <img src="./images/kodawari_photo1.jpg" >
            <p class="pt">湧水の里で使用する大豆は、道内の契約農家さんに栽培をお願いしています。特に、鶴の子品種の中でも、在来種で希少価値の高いユウヅルは幻の品種として名高い大豆です。その他北海道産の大豆をその年のできによりブレンドする長年培った職人の技が、当店の豆腐の美味しさの秘密です。</p>
        </section>
        <!-- kodawari1 -->

        <section class="kodawari2 pb_l mb_l">
            <h3 class="headline2 mb_l">大地が生み出したここだけの湧水</h3>
            <img src="./images/kodawari_photo2.jpg?v=20190129" >
            <p class="pt">豆腐の90％が水分であり、水が豆腐の味に大きく関係しています。当店の豆腐はミネラルやマグネシウムを多く含んだ、羊蹄山の伏流水を使用しています。地下から直接汲み入れるため、1年通じて水温が6℃。完成した豆腐をすぐ冷やせるため「豆腐が生きている」と評価してくれるお客様もございます。</p>
        </section>
        <!-- kodawari1 -->

        <section class="kodawari3 pb_l mb_l">
            <h3 class="headline2 mb_l">匠の技、職人の努力、日々探求</h3>
            <img src="./images/kodawari_photo3.jpg" >
            <p class="pt">先代の技をしっかりと引継ぎ、努力を怠らないこと、それが私たちの使命です。先代の技と、新しい考えを融合させ、よりよいものを。私たちは日々、旨い豆腐を試行錯誤し、探求しています。</p>
        </section>
        <!-- kodawari1 -->

        <section class="kodawari4 pb_l mb_l">
            <h3 class="headline2 mb_l">味の決め手、こだわりのにがり</h3>
            <img src="./images/kodawari_photo4.jpg" >
            <p class="pt">豆腐の味の決め手となる当店のにがりは、北海道道南の熊石の天然海水にがりを主に、複数のにがりをブレンドしています。豆乳とにがりが喧嘩をしない、そして大豆の味を引き出せるのが特徴です。</p>
        </section>
        <!-- kodawari1 -->

        <section class="kodawari5 pb_l mb_l">
            <h3 class="headline2 mb_l">驚きのメニュー数「豆腐で40種」</h3>
            <img src="./images/kodawari_photo5.jpg" >
            <p class="pt">豆腐の可能性は無限大をテーマに、商品開発を日々行っております。今では豆腐で40種類、スイーツで10種類の商品ラインナップとなりました。職人の熱い想いと遊び心、お客様に豆腐の新しい味や、食べ方を楽しんでもらいたい、そんな想いが私たちの原動力です。</p>
        </section>
        <!-- kodawari1 -->

        <section class="net_store pt_l mt pb_l mb">
            <p><a href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
        </section>
        <!-- net_store -->

      </div>
      <!-- wrapper -->


  </div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>