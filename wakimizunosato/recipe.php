<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "レシピ ｜【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_recipe" class="subpage drawer drawer--right drawer-close">

    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">

            <section class="recipe pt_l">
                <div class="wrapper">
                    <h3><span class="num">01</span><span class="text">うきふのごま豆腐餡</span></h3>
                    <div class="cf main_box">
                        <div class="left">
                            <img src="./images/recipe_main_photo.jpg">
                        </div>
                        <!-- left -->
                        <div class="right pt">
                            <p class="pt_l">使用する商品</p>
                            <h4>うきふ</h4>
                            <img class="pt_s pb_s" src="./images/recipe_sub_photo.jpg">
                            <p class="linkbtn2"><a href="http://webcheck3.x0.com/wakimizu/eccube/html/products/detail/7">ネットストアで購入する</a></p>
                        </div>
                        <!-- right -->
                    </div>
                    <!-- main_box -->
                    <div class="recipe_cont cf pb">
                        <div class="material">
                            <h5 class="headline">材料（１人前）</h5>
                            <dl class="cf">
                                <dt><span class="text">・うきふ</span></dt>
                                <dd>1/2丁</dd>
                            </dl>
                            <dl class="cf">
                                <dt><span class="text">・昆布出汁</span></dt>
                                <dd>150cc</dd>
                            </dl>
                            <dl class="cf">
                                <dt><span class="text">・ねりごま</span></dt>
                                <dd>大<span class="small">さじ</span>3</dd>
                            </dl>
                            <dl class="cf">
                                <dt><span class="text">・醬油</span></dt>
                                <dd>小<span class="small">さじ</span>2</dd>
                            </dl>
                            <dl class="cf">
                                <dt><span class="text">・みりん</span></dt>
                                <dd>小<span class="small">さじ</span>1</dd>
                            </dl>
                            <dl class="cf">
                                <dt><span class="text">・葛粉</span></dt>
                                <dd>小<span class="small">さじ</span>1</dd>
                            </dl>
                        </div>
                        <!-- material -->
                        <div class="flow">
                            <h5 class="headline">作り方</h5>
                            <dl class="cf">
                                <dt>手順１.</dt>
                                <dd>お皿にうきふをおき、お皿ごと温めた蒸し器にかける。</dd>
                            </dl>
                            <dl class="cf">
                                <dt>手順２.</dt>
                                <dd>鍋にその他の材料をいれ、だまがなくなるように丁寧にかき混ぜて<br>から中火にかける。</dd>
                            </dl>
                            <dl class="cf">
                                <dt>手順３.</dt>
                                <dd>常に木べらでかき混ぜながら、沸騰したら弱火にして５分練り続ける。</dd>
                            </dl>
                            <dl class="cf">
                                <dt>手順４.</dt>
                                <dd>うきふを蒸し器からおろし、練った餡をかける。</dd>
                            </dl>
                            <dl class="cf">
                                <dt>手順５.</dt>
                                <dd>お好みで山椒の葉を添える。</dd>
                            </dl>
                        </div>
                        <!-- flow -->
                    </div>
                    <!-- recipe_cont -->

                    <div class="introduction pt_l pb_l">
                        <div class="cf box">
                            <div class="left">
                                <h4 class="headline">レシピを紹介してくれた人</h4>
                                <h5>緑泉寺住職・料理僧<br class="sp"><span class="name">青江　覚峰</span>氏</h5>
                                <p>1977年東京生まれ。浄土真宗東本願寺派緑泉寺住職。カリフォルニア州立大学にてＭＢＡ取得。料理僧として料理、食育に取り組む。日本初・お寺発のブラインドレストラン「暗闇ごはん」代表。超宗派仏教徒によるインターネット寺院「彼岸寺」や世界最大級の寺社フェス「向源」など寺社メディアやイベントの運営を経て（株）なか道代表取締役就任。「世界一受けたい授業」（日本テレビ系列）などメディアでも活躍。著書に『お寺ごはん』（ディスカヴァー・トゥエンティワン）など</p>
                            </div>
                            <!-- left -->
                            <div class="right">
                                <img src="./images/recipe_introduction.jpg">
                            </div>
                        </div>
                        <!-- box -->

                    </div>
                </div>
                <!-- wrapper -->
            </section>
            <!-- recipe -->

            <section class="net_store pt_l mt_l pb_l mb_l">
                <div class="wrapper">
                    <p class="pb_l"><a href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
                </div>
                <!-- wrapper -->
            </section>
            <!-- net_store -->

        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
