<?php
  $url = $_SERVER['REQUEST_URI'];
  $tmp = explode("/", $url);
  $term_slug = end($tmp);
?>
<header id="top">
    <div class="pcmenu cf">
        <div class="header-top cf">
            <div class="wrapper">
                <div class="header-logo">
                    <p class="site">真狩豆腐工房 湧水の里 公式WEBサイト</p>
                    <h1 title="石窯パンマルシェ HARU">
                        <a href="<?php echo $root_path; ?>">
                            <img src="<?php echo $root_path; ?>images/header_logo.svg" alt="真狩豆腐工房 湧水の里">
                        </a>
                    </h1>
                </div>
                <!-- header-logo -->
                <div class="header-contact">
                    <ul>
                        <li class="top cf">
                            <a class="facebook" href="https://www.facebook.com/wakimizunosato/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>
                            <a class="instagram" href="https://www.instagram.com/wakimizunosato/?hl=ja" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a>
                        </li>
                    </ul>
                    <div class="tel">
                        0136-48-2636
                    </div>
                    <address>
                        営業時間【4月～10月】8：30～18：00<br>
                        【11月～3月】9：00～17：00<br>
                    </address>
                    <p class="rest">年中無休</p>
                </div>
                <!-- header-contact -->
            </div>
            <!-- wrapper -->
        </div>
        <!-- header-top -->
        <div class="header-bottom">
            <div class="wrapper">
                <nav>
                    <ul class="cf">
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>products.php">商品一覧</a></li>
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>shop.php">店舗案内・アクセス</a></li>
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>recipe.php">豆腐レシピ</a></li>
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>../eccube/html/">オンラインショップ</a></li>
                        <li><a class="hvr-underline-from-center" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                    </ul>
                </nav>
            </div>
            　
            <!-- wrapper -->
        </div>
        <!-- header-bottom -->
    </div>
    <!-- pcmenu -->

    <div class="spmenu drawermenu" role="banner" id="top">
        <h2 class="cf"><a href="<?php echo $root_path; ?>"><img src="<?php echo $root_path; ?>images/header_logo.svg" alt="真狩豆腐工房 湧水の里"><span class="text">真狩豆腐工房 湧水の里</span></a></h2>
        <button type="button" class="drawer-toggle drawer-hamburger"> <span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span>
            <img class="menu-icon" src="<?php echo $root_path; ?>images/header_menu.svg">
        </button>

        <nav class="drawer-nav" role="navigation">
            <div class="inner">
                <ul class="drawer-menu cf">
                    <li class="top cf">
                        <a class="instagram" href="#" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a>
                        <a class="facebook" href="#" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>
                    </li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>index.php">トップページ</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>products.php">商品一覧</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>shop.php">店舗案内・アクセス</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>recipe.php">豆腐レシピ</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>../eccube/html/">オンラインショップ</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
                <ul class="drawer-menu cf">
<!--
                    <li class="text"><a class="drawer-menu-item" href="#">採用情報</a></li>
                    <li class="text"><a class="drawer-menu-item" href="#">会社情報</a></li>-->
                    <li class="text"><a class="drawer-menu-item" href="#">プライバシーポリシー</a></li>
                </ul>

                <div class="store_info">
                    <img src="<?php echo $root_path; ?>images/spmenu_store.jpg">
                    <h3>真狩豆腐工房 湧水の里</h3>
                    <p class="ad"><a href="https://goo.gl/maps/XxYttFN2Tfk" target="_blank">北海道虻田郡真狩村２１７番地１</a></p>
                    <p x-ms-format-detection="none"><a href="tel:0136-48-2636">0136-48-2636</a></p>
                </div>
                <!-- store_info -->

            </div>
            <!--inner -->
        </nav>
    </div>
    <!-- spmenu -->
</header>
<main role="main">
