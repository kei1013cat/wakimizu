<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "商品一覧 ｜【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_products" class="subpage drawer drawer--right drawer-close">

<!-- Google Tag Manager (noscript) -->
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
  <?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">
      <div class="wrapper">
   
       <section id="products_menu" class="pt_l mt">
          <ul class="cf">
              <li class=""><a href="#product_grp1"><img src="./images/products/product_grp1.jpg" /><span>とうふ</span></a></li>
              <li class=""><a href="#"><img src="./images/products/product_grp2.jpg" /><span>変わりとうふ</span></a></li>
              <li class=""><a href="#product_grp3"><img src="./images/products/product_grp3.jpg" /><span>揚げ</span></a></li>
              <li class=""><a href="#"><img src="./images/products/product_grp4.jpg" /><span>湯葉・豆乳</span></a></li>
              <li class=""><a href="#"><img src="./images/products/product_grp5.jpg" /><span>スイーツ</span></a></li>
              <li class=""><a href="#"><img src="./images/products/product_grp6.jpg" /><span>ギフトセット</span></a></li>
          </ul>
      </section>
      <!-- products_menu -->
 
      <?php //*****************************************************************************************// ?>
      <?php // とうふ                                                                                      ?>
      <?php //*****************************************************************************************// ?>
      <section class="product_grp mt_l" id="product_grp1">
          <h3><img src="./images/products/product_grp1.jpg" ><span>とうふ</span><span class="line"></span></h3>
          <ul class="product cf">
              
              <?php //*****************************************************************************************// ?>
              <li class="item-col matchheight">
                <div class="item-outer">
                <h4>
                  すごいとうふ
                  <span class="cat_osusume">オススメ</span>
                  <span class="cat_new">NEW</span>
                  <span class="cat_ninki">人気！</span>
                </h4>
                <div class="bx-slider">
                  <div class="main">
                    <div class="outer">
                      <ul class="main-slider">
                        <li><img src="./images/products/product_grp1_item1_photo1.jpg" /></li>
                        <li><img src="./images/products/product_grp1_item1_photo2.jpg" /></li>
                        <li><img src="./images/products/product_grp1_item1_photo3.jpg" /></li>
                      </ul>
                    </div>
                    <!-- outer --> 
                  </div>
                  <!-- main -->
                  <div class="sub">
                    <ul class="bx-slider-thum" >
                      <li><a data-slide-index="0" href="#" class="act"><img src="./images/products/product_grp1_item1_photo1.jpg" /></a></li>
                      <li><a data-slide-index="1" href="#"><img src="./images/products/product_grp1_item1_photo2.jpg" /></a></li>
                      <li><a data-slide-index="2" href="#"><img src="./images/products/product_grp1_item1_photo3.jpg" /></a></li>
                    </ul>
                    <!-- /#slider --> 
                  </div>
                  <!-- sub --> 
                </div>
                <!-- bx-slider-->
                <div class="cont">
                    <h5>内容量　：　400g</h5>
                    <p>おすすめポイントおすすめポイントおすすめポイントおすすめポイントおすす
                    めポイントおすすめポイントおすすめポイントおすすめポイントおすすめポイ
                    ントおすすめポイントおすすめポイントおすすめポイントおすすめポイント</p>
                    <p class="linkbtn2"><a href="linkbtn2">ネットストアで購入する</a></p>
                </div>
                <!-- cont -->
                </div>
                <!-- item-outer -->
              </li>
              
              <?php //*****************************************************************************************// ?>
              <li class="item-col matchheight">
                <div class="item-outer">
                <h4>
                  すごいとうふ
                  <span class="cat_osusume">オススメ</span>
                  <span class="cat_new">NEW</span>
                  <span class="cat_ninki">人気！</span>
                </h4>
                <div class="bx-slider">
                  <div class="main">
                    <div class="outer">
                      <ul class="main-slider">
                        <li><img src="./images/products/product_grp1_item2_photo1.jpg" /></li>
                        <li><img src="./images/products/product_grp1_item2_photo2.jpg" /></li>
                        <li><img src="./images/products/product_grp1_item2_photo3.jpg" /></li>
                      </ul>
                    </div>
                    <!-- outer --> 
                  </div>
                  <!-- main -->
                  <div class="sub">
                    <ul class="bx-slider-thum" >
                      <li><a data-slide-index="0" href="#" class="act"><img src="./images/products/product_grp1_item2_photo1.jpg" /></a></li>
                      <li><a data-slide-index="1" href="#"><img src="./images/products/product_grp1_item2_photo2.jpg" /></a></li>
                      <li><a data-slide-index="2" href="#"><img src="./images/products/product_grp1_item2_photo3.jpg" /></a></li>
                    </ul>
                    <!-- /#slider --> 
                  </div>
                  <!-- sub --> 
                </div>
                <!-- bx-slider-->
                <div class="cont">
                    <h5>内容量　：　400g</h5>
                    <p>おすすめポイントおすすめポイントおすすめポイントおすすめポイントおすす
                    めポイントおすすめポイントおすすめポイントおすすめポイントおすすめポイ
                    ントおすすめポイントおすすめポイントおすすめポイントおすすめポイント</p>
                    <p class="linkbtn2"><a href="linkbtn2">ネットストアで購入する</a></p>
                </div>
                <!-- cont -->
                </div>
                <!-- item-outer -->
              </li>
              <?php //*****************************************************************************************// ?>

          </ul>
          <!-- product -->
      </section>
      <!-- product_grp1 -->
      <?php //*****************************************************************************************// ?>      

          
     <?php //*****************************************************************************************// ?>
      <?php // おあげ                                                                                      ?>
      <?php //*****************************************************************************************// ?>
      <section class="product_grp mt_l" id="product_grp3">
          <h3><img src="./images/products/product_grp3.jpg" ><span>おあげ</span><span class="line"></span></h3>
          <ul class="product cf">
              
              <?php //*****************************************************************************************// ?>
              <li class="item-col matchheight">
                <div class="item-outer">
                <h4>
                  すごいおあげ
                  <span class="cat_osusume">オススメ</span>
                  <span class="cat_new">NEW</span>
                  <span class="cat_ninki">人気！</span>
                </h4>
                <div class="bx-slider">
                  <div class="main">
                    <div class="outer">
                      <ul class="main-slider">
                        <li><img src="./images/products/product_grp3_item1_photo1.jpg" /></li>
                        <li><img src="./images/products/product_grp3_item1_photo2.jpg" /></li>
                        <li><img src="./images/products/product_grp3_item1_photo3.jpg" /></li>
                      </ul>
                    </div>
                    <!-- outer --> 
                  </div>
                  <!-- main -->
                  <div class="sub">
                    <ul class="bx-slider-thum" >
                      <li><a data-slide-index="0" href="#" class="act"><img src="./images/products/product_grp3_item1_photo1.jpg" /></a></li>
                      <li><a data-slide-index="1" href="#"><img src="./images/products/product_grp3_item1_photo2.jpg" /></a></li>
                      <li><a data-slide-index="2" href="#"><img src="./images/products/product_grp3_item1_photo3.jpg" /></a></li>
                    </ul>
                    <!-- /#slider --> 
                  </div>
                  <!-- sub --> 
                </div>
                <!-- bx-slider-->
                <div class="cont">
                    <h5>内容量　：　400g</h5>
                    <p>おすすめポイントおすすめポイントおすすめポイントおすすめポイントおすす
                    めポイントおすすめポイントおすすめポイントおすすめポイントおすすめポイ
                    ントおすすめポイントおすすめポイントおすすめポイントおすすめポイント</p>
                    <p class="linkbtn2"><a href="linkbtn2">ネットストアで購入する</a></p>
                </div>
                <!-- cont -->
                </div>
                <!-- item-outer -->
              </li>
              
              <?php //*****************************************************************************************// ?>
              <li class="item-col matchheight">
                <div class="item-outer">
                <h4>
                  すごいおあげ
                  <span class="cat_osusume">オススメ</span>
                  <span class="cat_new">NEW</span>
                  <span class="cat_ninki">人気！</span>
                </h4>
                <div class="bx-slider">
                  <div class="main">
                    <div class="outer">
                      <ul class="main-slider">
                        <li><img src="./images/products/product_grp3_item2_photo1.jpg" /></li>
                        <li><img src="./images/products/product_grp3_item2_photo2.jpg" /></li>
                        <li><img src="./images/products/product_grp3_item2_photo3.jpg" /></li>
                      </ul>
                    </div>
                    <!-- outer --> 
                  </div>
                  <!-- main -->
                  <div class="sub">
                    <ul class="bx-slider-thum" >
                      <li><a data-slide-index="0" href="#" class="act"><img src="./images/products/product_grp3_item2_photo1.jpg" /></a></li>
                      <li><a data-slide-index="1" href="#"><img src="./images/products/product_grp3_item2_photo2.jpg" /></a></li>
                      <li><a data-slide-index="2" href="#"><img src="./images/products/product_grp3_item2_photo3.jpg" /></a></li>
                    </ul>
                    <!-- /#slider --> 
                  </div>
                  <!-- sub --> 
                </div>
                <!-- bx-slider-->
                <div class="cont">
                    <h5>内容量　：　400g</h5>
                    <p>おすすめポイントおすすめポイントおすすめポイントおすすめポイントおすす
                    めポイントおすすめポイントおすすめポイントおすすめポイントおすすめポイ
                    ントおすすめポイントおすすめポイントおすすめポイントおすすめポイント</p>
                    <p class="linkbtn2"><a href="linkbtn2">ネットストアで購入する</a></p>
                </div>
                <!-- cont -->
                </div>
                <!-- item-outer -->
              </li>
              <?php //*****************************************************************************************// ?>

          </ul>
          <!-- product -->
      </section>
      <!-- product_grp1 -->
      <?php //*****************************************************************************************// ?>                
          

      <section class="net_store pt_l mt_l pb mb">
        <p><a  href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
      </section>
      <!-- net_store -->
    
    </div>
    <!-- wrapper -->

    <section class="reserv pt pb bg_beige">
    <div class="wrapper">
      <div class="outer cf">
          <div class="photo">
              <img src="./images/products_reserv_photo.png">
          </div>
          <!-- photo -->
          <div class="text">
              <h3 class="pt">店頭引取り予約（お取り置き）のご案内</h3>
              <p>
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                  テキストテキストテキストテキストテキストテキスト
              </p>
          </div>
          <!-- text -->
      </div>
    </div>
    </section>
    <!-- reserv -->

  </div>
  <!-- contents -->
  <?php include_once "bottom_link.php"; ?>
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>