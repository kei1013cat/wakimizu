<section id="bottom_link">
<div class="wrapper">
<div class="top pt_l pb_s"><img src="./images/bottom_link_title.png"></div>
<ul class="grid_col3 cf pb_l">

    <li class="col photo_title">
    <a href="<?php echo $root_path; ?>shop.php#access">
        <figure>
            <div class="photo"><img src="./images/bottom_link_photo1.jpg"></div>
            <figcaption class="center">アクセス</figcaption>
        </figure>
    </a></li>
    <li class="col photo_title">
    <a href="<?php echo $root_path; ?>kodawari.php">
        <figure>
            <div class="photo"><img src="./images/bottom_link_photo2.jpg" alt=""></div>
            <figcaption class="center">こだわり</figcaption>
        </figure>
    </a></li>
    <li class="col photo_title">
    <a href="<?php echo $root_path; ?>shop.php">
        <figure>
            <div class="photo"><img src="./images/bottom_link_photo3.jpg" alt=""></div>
            <figcaption class="center">店舗案内</figcaption>
        </figure>
    </a></li>
  </ul>
</div>
</section>