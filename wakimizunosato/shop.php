<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "店舗案内・アクセス    ｜【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";

?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once "head.php"; ?>
</head>

<body id="page_shop" class="subpage drawer drawer--right drawer-close">

    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->

    <div id="outer">
        <?php include_once "header.php"; ?>
        <?php include_once "pagetitle.php"; ?>
        <div id="contents">
            <section class="shop-top pb_l">
                <div class="wrapper">
                    <h3 class="mt_l mb"><img src="./images/shop_headtitle.svg" alt="真狩豆腐工房 湧水の里"></h3>
                    <p class="mb">店舗名　真狩豆腐工房　湧水の里<br class="sp">（まっかりとうふこうぼう わきみずのさと）</p>
                    <ul class="grid_col2 ptn2 cf pb">
                        <li class="col">
                            <img src="./images/shop_top_photo1.jpg">
                        </li>
                        <li class="col">
                            <img src="./images/shop_top_photo2.jpg">
                        </li>
                    </ul>
                    <address>
                        住所　北海道虻田郡真狩村217番地1<br>
                        <a href="tel:0136482636">電話　0136-48-2636</a><br>
                        FAX　&nbsp;0136-48-2637<br>
                        営業時間<br>
                        &nbsp;&nbsp;【4月～10月】8：30～18：00<br>
                        &nbsp;&nbsp;【11月～3月】9：00～17：00<br>
                        年中無休
                    </address>
                    <div class="map mt_l" id="access">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2928.2467616477734!2d140.77672231584347!3d42.783132979160854!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f130.1!3m3!1m2!1s0x5f0aad65b002945f%3A0x31b05da12002e856!2z5rmn5rC044Gu6YeMIOecn-eLqeixhuiFkOW3peaIvw!5e0!3m2!1sja!2sjp!4v1547116216475" frameborder="0" style="border:0" allowfullscreen></iframe>

                        <div class="access_bottom mt pb_s">
                            <div class="outer cf">
                                <div class="col1">
                                    自家用車で
                                </div>
                                <!-- col1 -->
                                <div class="col2">
                                    ●札幌から（中山峠経由）<br>
                                    車／国道230号線、道道66号線経由で2時間<br><br>

                                    ●小樽から<br>
                                    車／国道5号線、道道66号線経由で1時間40分<br><br>

                                    ●新千歳空港から（支笏湖経由）<br>
                                    車／道道16号線、国道276号線、国道230号線、道道66号線経由で2時間
                                </div>
                                <!-- col2 -->
                                <div class="col3">
                                    ●比羅夫（ヒラフ）エリアから<br>
                                    車／道道631号線、国道5号線、道道66号線経由で20分<br><br>

                                    ●JRニセコ駅から<br>
                                    車／道道66号線で12分
                                </div>
                                <!-- col3 -->
                            </div>
                            <!-- outer -->
                        </div>
                        <!-- access_bottom -->

                    </div>
                    <!-- map -->
                </div>
            </section>
            <!-- shop-top -->

            <section class="shop-about bg_beige pb_l">
                <div class="wrapper pb_l">
                    <h3 class="headline2 pt_l mb">湧水について</h3>
                    <img src="./images/shop_about_photo.jpg">
                    <p class="pt">
                        店舗前には、数十年の歳月をかけて生まれる、ミネラルたっぷりの真狩の水が岩肌から勢いよく湧き出しています。羊蹄山に降った雨や雪溶け水が生まれ変わった天然の水は、夏場でも冷たく、凛とした飲み口です。<br><br>
                        ※同敷地内の飲用可能な湧水は当店の管理ではございません。</p>
                </div>
            </section>
            <!-- shop-about -->

            <section class="net_store pt_l mt pb_l">
                <div class="wrapper">
                    <p class="pb"><a href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
                </div>
            </section>
            <!-- net_store -->

            <!--
            <section class="bnr pb">
                <div class="wrapper">
                    <a href="./contact.php">
                        <div class="bnr_outer cf">
                            <div class="top_box"></div>
                            <div class="photo"></div>
                            <div class="bnr">
                                お問合わせバナー
                            </div>
                            <div class="bottom_box"></div>
                        </div>
                    </a>
                </div>
            </section>-->

        </div>
        <!-- contents -->

        <?php include_once "footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
