<?php require('../cms/wp-load.php'); ?>

<?php
$root_path = "./";
$title = "お問い合わせ｜【公式】真狩豆腐工房 湧水の里";
$description = "";
$keyword = "";
$privacy_link = "";

?>
<!DOCTYPE html>
<html lang="ja">
<head>
<?php include_once "head.php"; ?>
</head>

<body id="page_contact" class="subpage drawer drawer--right drawer-close">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZGXWVJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="outer">
<?php include_once "header.php"; ?>
  <?php include_once "pagetitle.php"; ?>
  <div id="contents">

	<section class="contact bg_beige">
		<div class="wrapper">
			<h3 class="headline1 mt_l pb_l">お問い合わせフォーム</h3>
			<form class="mb_s" id="mailformpro" action="./form/mailformpro/mailformpro.cgi" method="POST">
			<p class="text fsize">お問い合わせはこちらのフォームからお気軽にどうぞ</p>
			<table class="style1 mt_s form" border="0">
				<tr>
					<th scope="row"><em>必須</em>お名前</th>
					<td><input type="text" name="お名前" value="" size="40" maxlength="60" required /></td>
				</tr>
				<tr>
					<th scope="row"><em>必須</em>お電話</th>
					<td><input type="text" name="お電話" value="" size="40" maxlength="60" required /></td>
				</tr>
				<tr>
					<th scope="row"><em>必須</em>メールアドレス</th>
					<td><input type="text" data-type="email" data-parent="mailfield" name="email" size="40" maxlength="60" required /><br>
                        <p class="memo">※半角英数字<br></p>
				</tr>
				<tr>
					<th scope="row"><em>必須</em>お問合わせ内容</th>
					<td><textarea name="お問合わせ内容" cols="42" rows="15" required placeholder="お問い合わせやご質問など入力"></textarea></td>
				</tr>

			</table>
            
			<div class="mfp_buttons pb_l pt_s">
				<button type="submit">入力内容を確認する</button>
			</div>
			
			</form>
			<script type="text/javascript" id="mfpjs" src="form/mailformpro/mailformpro.cgi" charset="UTF-8"></script>
		</div>
		<!-- wrapper -->
	</section>
    <section class="net_store pt_l mt pb_l mb">
        <div class="wrapper">
        <p><a href="<?php echo $root_path; ?>../eccube/html/"><img src="./images/btn_netstore.png" alt="真狩豆腐工房の商品も、買える！ | 湧水の里 ネットストア"></a></p>
        </div>
        <!-- wrapper -->
    </section>
    <!-- net_store -->		
	</div>
  <!-- contents -->
  
  <?php include_once "footer.php"; ?>
</div>
<!-- outer -->

</body>
</html>