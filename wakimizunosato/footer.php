<footer>
    <div class="footer-top">
        <div class="wrapper">
            <nav>
                <ul class="sp cf">
                    <li class="sp"><a href="<?php echo $root_path; ?>index.php">トップページ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>kodawari.php">こだわり</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>products.php">商品一覧</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>recipe.php">豆腐レシピ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>onlineshop.php">オンラインショップ</a></li>
                    <li class="sp"><a href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
                <ul class="comp cf">
                    <!--
                    <li class="text"><a href="#">会社情報</a></li>
                    <li class="text"><a href="#">採用情報</a></li>-->
                    <li class="text"><a href="#">プライバシーポリシー</a></li>
                    <li class="pc text"><a href="#">オンラインショップガイド</a></li>
                    <li class="pc text"><a href="<?php echo $root_path; ?>contact.php">お問い合わせ</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- footer-top -->
    <div class="footer-bottom">
        <div class="wrapper">
            <div class="cf">
                <h3 class="footer-logo">
                    <a href="<?php echo $root_path; ?>"><img src="<?php echo $root_path; ?>images/footer_logo.svg"></a>
                </h3>
                <div class="footer-contact">
                    <div class="tel">
                        0136-48-2636
                    </div>
                    <address>
                        営業時間【4月～10月】8：30～18：00<br>
                        【11月～3月】9：00～17：00<br>
                    </address>
                    <p class="rest">年中無休</p>
                    <ul>
                        <li class="top cf">
                            <a class="facebook" href="https://www.facebook.com/wakimizunosato/" target="_blank"><img src="<?php echo $root_path; ?>images/header_facebook.png"></a>
                            <a class="instagram" href="https://www.instagram.com/wakimizunosato/?hl=ja" target="_blank"><img src="<?php echo $root_path; ?>images/header_instagram.png"></a>
                        </li>
                    </ul>
                </div>
                <!-- header-contact -->
            </div>
            <!-- cf -->
        </div>
        <!-- wrapper -->
    </div>
    <!-- footer-bottom -->
    <p class="copy">Copyrights <?php echo date("Y"); ?> wakimizunosato</p>

</footer>

<div id="page_top">

    <a href="#"><img src="<?php echo $root_path; ?>images/top.svg" alt="pagetop"></a>
</div>

<script type="text/javascript" src="<?php echo $root_path; ?>js/top.js"></script>

</main>
