<?php
$keys = parse_url($_SERVER["REQUEST_URI"]);
$str = explode("/", $keys['path']);
$url = preg_replace('/\.[^.]+$/','',end($str));
?>
<div id="pagetitle">
    <div class="photo <?php echo $url; ?>" style="background:url(<?php echo $root_path; ?>images/pagetitle_<?php echo $url; ?>.jpg) no-repeat;">
        <nav class="nav-onlineshop">
            <div class="wrapper">
                <ul class="cf">
                    <li><a href="<?php echo $root_path; ?>../eccube/html/mypage/login">ログイン</a></li>
                    <li><a href="<?php echo $root_path; ?>../eccube/html/cart">カート</a></li>
                    <li><a href="<?php echo $root_path; ?>../eccube/html/help/tradelaw">特商法の表記</a></li>
                    <li><a href="<?php echo $root_path; ?>../eccube/html/help/guide">ご利用ガイド</a></li>
                    <li><a href="#">送料について</a></li>
                </ul>
            </div>
        </nav>
        <div class="wrapper">
        <div class="img-onlineshop">
            <img class="left" src="<?php echo $root_path; ?>images/onlineshop_img1.png">
            <img class="right" src="<?php echo $root_path; ?>images/onlineshop_img2.png">
        </div>
        
        <h2>       
        <?php if($url=="kodawari"): ?>
            こだわり
        <?php elseif($url=="products"): ?>
            商品一覧
        <?php elseif($url=="recipe"): ?>
            レシピ
        <?php elseif($url=="shop"): ?>
            店舗案内
        <?php elseif($url=="contact"): ?>
            お問い合わせ
        <?php elseif($url=="thanks"): ?>
            送信完了
        <?php else: ?>
            オンラインショップ
        <?php endif; ?>
        </h2>
        </div>
    </div>
</div>
<!-- pagetitle -->