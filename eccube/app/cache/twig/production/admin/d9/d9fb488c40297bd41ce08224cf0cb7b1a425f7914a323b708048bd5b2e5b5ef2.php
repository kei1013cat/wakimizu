<?php

/* __string_template__bb8a4a20b1fc5fafc7d78c4ddae70ba741ad09a1bb9df81f5280dc317c0f3df0 */
class __TwigTemplate_2ce58b8a6ead46eb5bffaef4f2dfdfb1030321a45dc1b1478b119089f3a56215 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 22
        $this->parent = $this->loadTemplate("default_frame.twig", "__string_template__bb8a4a20b1fc5fafc7d78c4ddae70ba741ad09a1bb9df81f5280dc317c0f3df0", 22);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default_frame.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 24
        $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "Form/bootstrap_3_horizontal_layout.html.twig"));
        // line 22
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 26
    public function block_title($context, array $blocks = array())
    {
        echo "パスワード変更";
    }

    // line 28
    public function block_main($context, array $blocks = array())
    {
        // line 29
        echo "<div class=\"row\" id=\"aside_wrap\">
    <div id=\"detail_wrap\" class=\"col-md-9\">
        <div id=\"detail_box__body\" class=\"box\">
            <div id=\"detail_box__body_header\" class=\"box-header\">
                <h3 class=\"box-title\">パスワード変更</h3>
            </div><!-- /.box-header -->
            <form role=\"form\" class=\"form-horizontal\" name=\"form1\" id=\"form1\" method=\"post\" action=\"\" enctype=\"multipart/form-data\">
                <div id=\"detail_box__body_inner\" class=\"box-body\">
                    ";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token", array()), 'row');
        echo "
                    ";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "current_password", array()), 'row', array("type" => "password", "value" => ""));
        echo "
                    ";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "change_password", array()), "first", array()), 'row', array("type" => "password", "value" => ""));
        echo "
                    ";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "change_password", array()), "second", array()), 'row', array("type" => "password", "value" => ""));
        echo "
                    <div class=\"extra-form\">
                        ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "getIterator", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["f"]) {
            // line 43
            echo "                            ";
            if (preg_match("[^plg*]", $this->getAttribute($this->getAttribute($context["f"], "vars", array()), "name", array()))) {
                // line 44
                echo "                                ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["f"], 'row');
                echo "
                            ";
            }
            // line 46
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['f'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "                    </div>

                </div><!-- /.box-body -->
            </form>
        </div><!-- /.box -->
    </div><!-- /.col -->

    <div class=\"col-md-3\" id=\"aside_column\">
        <div id=\"common_box\" class=\"col_inner\">
            <div id=\"common_button_box\" class=\"box no-header\">
                <div id=\"common_button_box__body\" class=\"box-body\">
                    <div id=\"common_button_box__button_area\" class=\"row text-center\">
                        <div id=\"common_button_box__insert_button\" class=\"col-sm-6 col-sm-offset-3 col-md-12 col-md-offset-0\">
                            <button class=\"btn btn-primary btn-block btn-lg\" onclick=\"document.form1.submit(); return false;\">変更</button>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.col -->
</div>
";
    }

    public function getTemplateName()
    {
        return "__string_template__bb8a4a20b1fc5fafc7d78c4ddae70ba741ad09a1bb9df81f5280dc317c0f3df0";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 47,  81 => 46,  75 => 44,  72 => 43,  68 => 42,  63 => 40,  59 => 39,  55 => 38,  51 => 37,  41 => 29,  38 => 28,  32 => 26,  28 => 22,  26 => 24,  11 => 22,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__bb8a4a20b1fc5fafc7d78c4ddae70ba741ad09a1bb9df81f5280dc317c0f3df0", "");
    }
}
