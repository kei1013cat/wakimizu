<?php

/* __string_template__96debed6568b4283f775b957dbe0d085b89a27f9bd44be09789e44e76b28da7c */
class __TwigTemplate_ae077d774b747ac60567290cd795096e7c84b7b2881c58f54f4cd2514a258db7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 10
        echo "
<!-- ▼item_list▼ -->
<div id=\"item_list\">
    <div class=\"row cf\">
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["recommend_products"]) ? $context["recommend_products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["RecommendProduct"]) {
            // line 15
            echo "                <div class=\"pickup_item\">
                    <div class=\"pickup_outer\">
                    <a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_detail", array("id" => $this->getAttribute($this->getAttribute($context["RecommendProduct"], "Product", array()), "id", array()))), "html", null, true);
            echo "\">
                        <dl>
                            <dt class=\"item_name\">
                                    ";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["RecommendProduct"], "Product", array()), "name", array()), "html", null, true);
            echo "
                                    <div class=\"tag\">
                                        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["RecommendProduct"], "Product", array()), "ProductTag", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["ProductTag"]) {
                // line 23
                echo "                                            <span id=\"product_tag_box__product_tag--";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["ProductTag"], "Tag", array()), "id", array()), "html", null, true);
                echo "\" class=\"product_tag_list\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["ProductTag"], "Tag", array()), "html", null, true);
                echo "</span>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ProductTag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                                    </div>
                            </dt>
                            <dd class=\"item_photo\">
                                <div class=\"item_photo\"><img src=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "config", array()), "image_save_urlpath", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getNoImageProduct($this->getAttribute($this->getAttribute($context["RecommendProduct"], "Product", array()), "mainFileName", array())), "html", null, true);
            echo "\"></div>
                            </dd>
                        </dl>
                    </a>
                    </div>    
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['RecommendProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "    </div>
</div>
<!-- ▲item_list▲ -->";
    }

    public function getTemplateName()
    {
        return "__string_template__96debed6568b4283f775b957dbe0d085b89a27f9bd44be09789e44e76b28da7c";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 35,  64 => 28,  59 => 25,  48 => 23,  44 => 22,  39 => 20,  33 => 17,  29 => 15,  25 => 14,  19 => 10,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__96debed6568b4283f775b957dbe0d085b89a27f9bd44be09789e44e76b28da7c", "");
    }
}
