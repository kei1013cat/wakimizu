<?php

/* __string_template__ff74f38f4a8b1a4f7a508b8d14e92a705f520207beae941cb724c33d2849e453 */
class __TwigTemplate_6f1e2ed70bd93837170527564574b3de9be6053d2584f5b63dcf8ca464a83d0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 22
        $this->parent = $this->loadTemplate("default_frame.twig", "__string_template__ff74f38f4a8b1a4f7a508b8d14e92a705f520207beae941cb724c33d2849e453", 22);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default_frame.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 24
    public function block_main($context, array $blocks = array())
    {
        // line 25
        echo "    <div id=\"contents\" class=\"main_only\">
        <div id=\"guide_wrap\" class=\"container-fluid inner no-padding\">
            <div id=\"main\">
                <h1 class=\"page-heading\">ご利用ガイド</h1>
                <div id=\"guide_box__body\"  class=\"container-fluid\">
                    <div id=\"guide_box__body_inner\" class=\"row\">
                        <div id=\"guide_box__body_item\" class=\"col-md-10 col-md-offset-1\">


                        </div><!-- /.col -->
                    </div><!-- /.row -->

                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "__string_template__ff74f38f4a8b1a4f7a508b8d14e92a705f520207beae941cb724c33d2849e453";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 25,  28 => 24,  11 => 22,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__ff74f38f4a8b1a4f7a508b8d14e92a705f520207beae941cb724c33d2849e453", "");
    }
}
