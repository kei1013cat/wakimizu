<?php

/* __string_template__128396eebbb87b4174af08cf3fb31ad89bf9efc9e43c84da0fdcae761ffdf330 */
class __TwigTemplate_0f446a6178c78f04f99c477cce85bd492a1ab4dde40c3984037539bed7c384ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 22
        $this->parent = $this->loadTemplate("default_frame.twig", "__string_template__128396eebbb87b4174af08cf3fb31ad89bf9efc9e43c84da0fdcae761ffdf330", 22);
        $this->blocks = array(
            'javascript' => array($this, 'block_javascript'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default_frame.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 24
        $context["body_class"] = "front_page";
        // line 22
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 26
    public function block_javascript($context, array $blocks = array())
    {
        // line 27
        echo "<script>
\$(function(){
    \$('.main_visual').slick({
        dots: true,
        arrows: false,
        autoplay: true,
        speed: 300
    });
});
</script>
";
    }

    // line 42
    public function block_main($context, array $blocks = array())
    {
        // line 43
        echo "    <h3 class=\"headline5\"><span>商品カテゴリ</span></h3>
    <section id=\"products_menu\" class=\"pt_l mt mb_l pb_l\">
      <ul class=\"cf\">
          <li class=\"\"><a href=\"";
        // line 46
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=7\"><img src=\"../../wakimizunosato/images/products/product_grp1.jpg\" /><span>とうふ</span></a></li>
          <li class=\"\"><a href=\"";
        // line 47
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=7\"><img src=\"../../wakimizunosato/images/products/product_grp2.jpg\" /><span>変わりとうふ</span></a></li>
          <li class=\"\"><a href=\"";
        // line 48
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=12\"><img src=\"../../wakimizunosato/images/products/product_grp3.jpg\" /><span>揚げ</span></a></li>
          <li class=\"\"><a href=\"";
        // line 49
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=14\"><img src=\"../../wakimizunosato/images/products/product_grp4.jpg\" /><span>湯葉・豆乳</span></a></li>
          <li class=\"\"><a href=\"";
        // line 50
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=13\"><img src=\"../../wakimizunosato/images/products/product_grp5.jpg\" /><span>スイーツ</span></a></li>
          <li class=\"\"><a href=\"";
        // line 51
        echo $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_list");
        echo "?category_id=15\"><img src=\"../../wakimizunosato/images/products/product_grp6.jpg\" /><span>ギフトセット</span></a></li>
      </ul>
    </section>
    <!-- products_menu -->    

    <h3 class=\"headline4\"><img src=\"../../wakimizunosato/images/products/product_osusume.jpg\" ><span>おすすめ商品</span><span class=\"line\"></span></h3>

    <!-- ▼item_list▼ -->
    <div id=\"item_list\" class=\"pt_s\">
        <div class=\"row no-padding\">
            ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pagination"]) ? $context["pagination"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["Product"]) {
            // line 62
            echo "                <div id=\"result_list_box--";
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
            echo "\" class=\"item-outer col-sm-4 col-xs-6\">
                    <div id=\"result_list__item--";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
            echo "\" class=\"product_item\">
                        <a href=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getUrl("product_detail", array("id" => $this->getAttribute($context["Product"], "id", array()))), "html", null, true);
            echo "\">
                            <div id=\"result_list__image--";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
            echo "\" class=\"item_photo\">
                                <img src=\"";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "config", array()), "image_save_urlpath", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getNoImageProduct($this->getAttribute($context["Product"], "main_list_image", array())), "html", null, true);
            echo "\">
                            </div>
                            <dl id=\"result_list__detail--";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
            echo "\">
                                <dt id=\"result_list__name--";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
            echo "\" class=\"item_name\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "name", array()), "html", null, true);
            echo "</dt>
                                ";
            // line 70
            if ($this->getAttribute($context["Product"], "description_list", array())) {
                // line 71
                echo "                                    <dd id=\"result_list__description_list--";
                echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
                echo "\" class=\"item_comment\">";
                echo nl2br($this->getAttribute($context["Product"], "description_list", array()));
                echo "</dd>
                                ";
            }
            // line 73
            echo "                                ";
            if ($this->getAttribute($context["Product"], "hasProductClass", array())) {
                // line 74
                echo "                                    ";
                if (($this->getAttribute($context["Product"], "getPrice02Min", array()) == $this->getAttribute($context["Product"], "getPrice02Max", array()))) {
                    // line 75
                    echo "                                    <dd id=\"result_list__price02_inc_tax--";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
                    echo "\" class=\"item_price\">
                                        ";
                    // line 76
                    echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getPriceFilter($this->getAttribute($context["Product"], "getPrice02IncTaxMin", array())), "html", null, true);
                    echo "
                                    </dd>
                                    ";
                } else {
                    // line 79
                    echo "                                    <dd id=\"result_list__price02_inc_tax--";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
                    echo "\" class=\"item_price\">
                                        ";
                    // line 80
                    echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getPriceFilter($this->getAttribute($context["Product"], "getPrice02IncTaxMin", array())), "html", null, true);
                    echo " ～ ";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getPriceFilter($this->getAttribute($context["Product"], "getPrice02IncTaxMax", array())), "html", null, true);
                    echo "
                                    </dd>
                                    ";
                }
                // line 83
                echo "                                ";
            } else {
                // line 84
                echo "                                    <dd id=\"result_list__price02_inc_tax--";
                echo twig_escape_filter($this->env, $this->getAttribute($context["Product"], "id", array()), "html", null, true);
                echo "\" class=\"item_price\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('Eccube\Twig\Extension\EccubeExtension')->getPriceFilter($this->getAttribute($context["Product"], "getPrice02IncTaxMin", array())), "html", null, true);
                echo "</dd>
                                ";
            }
            // line 86
            echo "                            </dl>
                        </a>
                        ";
            // line 88
            if ( !twig_test_empty($this->getAttribute($context["Product"], "ProductTag", array()))) {
                // line 89
                echo "                            <!--▼商品タグ-->
                            <div id=\"product_tag_box\" class=\"product_tag\">
                                ";
                // line 91
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["Product"], "ProductTag", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["ProductTag"]) {
                    // line 92
                    echo "                                    <span id=\"product_tag_box__product_tag--";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["ProductTag"], "Tag", array()), "id", array()), "html", null, true);
                    echo "\" class=\"product_tag_list\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["ProductTag"], "Tag", array()), "html", null, true);
                    echo "</span>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ProductTag'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "                            </div>
                            <!--▲商品タグ-->
                        ";
            }
            // line 97
            echo "                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['Product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "        </div>

    </div>
    <!-- ▲item_list▲ -->

";
    }

    public function getTemplateName()
    {
        return "__string_template__128396eebbb87b4174af08cf3fb31ad89bf9efc9e43c84da0fdcae761ffdf330";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 100,  209 => 97,  204 => 94,  193 => 92,  189 => 91,  185 => 89,  183 => 88,  179 => 86,  171 => 84,  168 => 83,  160 => 80,  155 => 79,  149 => 76,  144 => 75,  141 => 74,  138 => 73,  130 => 71,  128 => 70,  122 => 69,  118 => 68,  111 => 66,  107 => 65,  103 => 64,  99 => 63,  94 => 62,  90 => 61,  77 => 51,  73 => 50,  69 => 49,  65 => 48,  61 => 47,  57 => 46,  52 => 43,  49 => 42,  35 => 27,  32 => 26,  28 => 22,  26 => 24,  11 => 22,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__128396eebbb87b4174af08cf3fb31ad89bf9efc9e43c84da0fdcae761ffdf330", "");
    }
}
